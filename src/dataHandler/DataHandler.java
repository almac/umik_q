package dataHandler;

import java.text.SimpleDateFormat;
import java.util.Date;

import frame.OfficeApp;

public class DataHandler {

	/** opisuje status z jakim zostal obsluzony ostatni klient */
	public boolean clientServedStatus = true;
	/** opisuje status polaczenia z InformationApp */
	public boolean clientConnected = true;
	/** obecnie obslugiwany numer */
	public String currNum;
	/** nastepny numer do obslugi */
	public String nextNum;
	/** liczba oczekujacych klientow */
	public String waiting;
	/** liczba obsluzonych klientow */
	public String served = "0";
	/** liczba nieobsluzonych klientow */
	public String nonServed = "0";
	/** sredni czas obslugi klienta */
	public String time = "5:00";
	/** obiekt wywolujacy dataHandlera */
	private OfficeApp officeApp;
	/** obecny czas oblsugi */
	private String currentTime = "0";
	/** czas przyjscia klienta */
	private String pastTime = "0";
	/** poczatkowy sredni czas */
	private int averageTime = 0;
	/** formater daty */
	private final SimpleDateFormat dt;

	/**
	 * Klasa agregujaca dane
	 * 
	 * @param _officeApp
	 *            obiekt OfficeApp do ktorego jest powiazany dataHandler
	 */
	public DataHandler(OfficeApp _officeApp) {
		officeApp = _officeApp;
		dt = new SimpleDateFormat("HH:mm:ss");
		pastTime = currentTime = dt.format(new Date()).toString();
	}

	/**
	 * odswiezenie danych przechowywanych w dataHandlerze po zmianie modelu itp.
	 */
	public void update() {
		if (OfficeApp.getDataModel().getCaseVec().size() != 0) {

			currNum = Integer.toString(OfficeApp.getDataModel().getCaseVec().get(0).getNumber());

			if (OfficeApp.getDataModel().getCaseVec().size() >= 2) {
				nextNum = Integer.toString(OfficeApp.getDataModel().getCaseVec().get(1).getNumber());
				waiting = Integer.toString(OfficeApp.getDataModel().getCaseVec().size() - 1);
			} else {
				nextNum = "--";
				waiting = "0";
			}

		} else {
			currNum = "--";
			nextNum = "--";
			waiting = "0";

		}

		officeApp.refreshOfficeAppData(currNum, served, nonServed, waiting);
	}

	/**
	 * oblicza nowy czas obslugi klienta
	 */
	public void newTime() {
		int serv = Integer.parseInt(served);
		int nonServ = Integer.parseInt(nonServed);
		if (OfficeApp.getDataModel().getCaseVec().size() != 0) {
			if (clientServedStatus) {
				served = Integer.toString(serv + 1);
				time = timeDifference(serv);
			} else {
				nonServed = Integer.toString(nonServ + 1);
			}
		}
	}

	/** odswiezenie danych dataHandlera po zakonczeniu obslugi klienta */
	public void nextClickUpdate() {
		int serv = Integer.parseInt(served);
		int nonServ = Integer.parseInt(nonServed);
		if (OfficeApp.getDataModel().getCaseVec().size() != 0) {
			OfficeApp.getDataModel().getCaseVec().remove(0);
			OfficeApp.getDataModel().fireTableDataChanged();

			int size = OfficeApp.getDataModel().getCaseVec().size();

			if (size != 0)
				currNum = Integer.toString(OfficeApp.getDataModel().getCaseVec().get(0).getNumber());
			else
				currNum = "--";

			if (size >= 2) {
				nextNum = Integer.toString(OfficeApp.getDataModel().getCaseVec().get(1).getNumber());
				waiting = Integer.toString(size - 1);
			} else {
				nextNum = "--";
				waiting = "0";
			}

		} else {
			currNum = "--";
			nextNum = "--";
			waiting = "0";

		}
	}

	/**
	 * funkcja liczaca sredni czas obslugi
	 * 
	 * @param served
	 *            liczba obsluzonych klientow
	 * @return sredni czas obslugi
	 */
	private String timeDifference(int served) {
		pastTime = currentTime;
		currentTime = dt.format(new Date()).toString();
		String[] current = currentTime.split(":");
		String[] past = pastTime.split(":");
		int difference = Integer.parseInt(current[0]) * 3600 + Integer.parseInt(current[1]) * 60 + Integer.parseInt(current[2])
				- (Integer.parseInt(past[0]) * 3600 + Integer.parseInt(past[1]) * 60 + Integer.parseInt(past[2]));

		System.out.println("avgT:  " + averageTime);
		int newAverage = 0;
		if (served != 0) {
			newAverage = ((served) * averageTime + difference) / (served + 1);
			averageTime = newAverage;
		} else {
			newAverage = difference;
			averageTime = newAverage;
		}

		String time = String.format("%02d" + ":" + "%02d", (int) Math.floor(newAverage / 60), newAverage % 60);
		return time;
	}

	public boolean isClientServedStatus() {
		return clientServedStatus;
	}

	public void setClientStatusServed(boolean clientServed) {
		this.clientServedStatus = clientServed;
	}

	public boolean isClientConnected() {
		return clientConnected;
	}

	public void setClientConnected(boolean clientConnected) {
		this.clientConnected = clientConnected;
	}

}
