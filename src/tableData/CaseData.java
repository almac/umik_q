package tableData;

public class CaseData {

	private long idTicket;
	private int number;
	private String caseTitle;
	private String name;
	private String surname;
	private String data1;
	private String data2;
	private String data3;

	/**
	 * Konstruktor pojedynczej sprawy ktora zostala zgloszona przez klienta na
	 * glowny serwer aplikacji i dotarla do odpowiedniego miejsca kolejkowania
	 * 
	 * @param idTicket
	 *            numer kolejnego zgloszenia
	 * @param number
	 *            numer klienta
	 * @param cases
	 *            sprawa jaka chce zalatwic klient
	 * @param name
	 *            imie klienta
	 * @param surname
	 *            nazwisko klienta
	 * @param data1
	 *            dodatkowe dane
	 * @param data2
	 *            dodatkowe dane
	 * @param data3
	 *            dodatkowe dane
	 */
	public CaseData(long idTicket, int number, String caseTitle, String name, String surname, String data1, String data2, String data3) {
		super();
		this.idTicket = idTicket;
		this.number = number;
		this.caseTitle = caseTitle;
		this.name = name;
		this.surname = surname;
		this.data1 = data1;
		this.data2 = data2;
		this.data3 = data3;
	}

	public int getNumber() {
		return number;
	}

	public String getCaseTitle() {
		return caseTitle;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public long getIdTicket() {
		return idTicket;
	}

	public String getData1() {
		return data1;
	}

	public String getData2() {
		return data2;
	}

	public String getData3() {
		return data3;
	}

}
