package frame;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import network.MainServerClient;
import network.Server;
import panels.OfficeAppControlPanel;
import tableModel.CaseModel;
import dataHandler.DataHandler;
import dialogs.NextClientDialog;

public class OfficeApp extends JFrame {

	/* obiekt agregujacy dane */
	public static DataHandler dataHandler;
	/** panel do dodawania obiektow graficznych */
	private JPanel contentPane;
	/** tabela przechowujaca obecne sprawy */
	private JTable caseTable;
	/** panel obslugujacy guziki */
	private JPanel buttonPanel;
	/** guzik nastepnego klienta */
	private JButton bNext;
	/** model obslugi spraw uzywany przez tabele */
	private static CaseModel dataModel;
	/** czesc sieciowa do obslugi InformationApp */
	private static Server server;
	/** panel obslugujacy prezentacje danych */
	public OfficeAppControlPanel controlPanel;
	/** czesc sieciowa do obslugi glownegoServera */
	private MainServerClient mainServerClient;
	private JButton btnPolacz;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OfficeApp frame = new OfficeApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OfficeApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 711, 459);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 274, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 263, 0, 0 };
		gbl_contentPane.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 0;
		contentPane.add(scrollPane, gbc_scrollPane);
		dataHandler = new DataHandler(this);
		dataModel = new CaseModel();

		caseTable = new JTable();
		caseTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		caseTable.getTableHeader().setReorderingAllowed(false);
		caseTable.setModel(dataModel);
		scrollPane.setViewportView(caseTable);
		
	//	dataHandler.update();
		
		controlPanel = new OfficeAppControlPanel();
		GridBagConstraints gbc_controlPanel = new GridBagConstraints();
		gbc_controlPanel.insets = new Insets(0, 0, 5, 5);
		gbc_controlPanel.fill = GridBagConstraints.BOTH;
		gbc_controlPanel.gridx = 0;
		gbc_controlPanel.gridy = 0;
		contentPane.add(controlPanel, gbc_controlPanel);

		dataHandler.update();

		buttonPanel = new JPanel();
		GridBagConstraints gbc_buttonPanel = new GridBagConstraints();
		gbc_buttonPanel.insets = new Insets(0, 0, 0, 5);
		gbc_buttonPanel.fill = GridBagConstraints.BOTH;
		gbc_buttonPanel.gridx = 0;
		gbc_buttonPanel.gridy = 1;
		contentPane.add(buttonPanel, gbc_buttonPanel);
		GridBagLayout gbl_buttonPanel = new GridBagLayout();
		gbl_buttonPanel.columnWidths = new int[] { 110, 110, 0 };
		gbl_buttonPanel.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_buttonPanel.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_buttonPanel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		buttonPanel.setLayout(gbl_buttonPanel);

		server = new Server();
		Thread t = new Thread(server);
		t.start();

		bNext = new JButton("Nastepny");
		bNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JDialog dialog = new NextClientDialog(OfficeApp.this);
				dialog.setLocationRelativeTo(null);
				dialog.setResizable(false);
				dialog.setVisible(true);
			}

		});
		GridBagConstraints gbc_bNext = new GridBagConstraints();
		gbc_bNext.insets = new Insets(0, 0, 5, 0);
		gbc_bNext.gridwidth = 2;
		gbc_bNext.gridx = 0;
		gbc_bNext.gridy = 0;
		buttonPanel.add(bNext, gbc_bNext);
		
		btnPolacz = new JButton("Polacz");
		btnPolacz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					mainServerClient = new MainServerClient();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		GridBagConstraints gbc_btnPolacz = new GridBagConstraints();
		gbc_btnPolacz.insets = new Insets(0, 0, 0, 5);
		gbc_btnPolacz.gridx = 0;
		gbc_btnPolacz.gridy = 3;
		buttonPanel.add(btnPolacz, gbc_btnPolacz);

	}

	/**
	 * obluga wydarzen po nacisnieeciu klawisza Nastepny
	 */
	public void nextClicked() {

		dataHandler.newTime();
		// przeslanie danych na MainServer
		mainServerClient.service.sendNextNumber();
		dataHandler.nextClickUpdate();
		// ustawiam lokalnie
		refreshOfficeAppData(dataHandler.currNum, dataHandler.served, dataHandler.nonServed, dataHandler.waiting);

		// wysylam do "okienka"
		sendToInformationApp();

	}

	/**
	 * funkcja odswiezajaca dane w panelu prezentacji danych
	 * 
	 * @param currNum
	 *            nowy obecnie obslugiwany numer
	 * @param served
	 *            nowa liczba obsluzonych klientow
	 * @param nonServed
	 *            nowa liczba nieobsluzonych klientow
	 * @param waiting
	 *            nowa liczba oczekujacych klientow
	 */
	public void refreshOfficeAppData(String currNum, String served, String nonServed, String waiting) {
		controlPanel.getLblCurrNumber().setText(currNum);
		controlPanel.getLblClientsServed().setText(served);
		controlPanel.getLblClientsNonServed().setText(nonServed);
		controlPanel.getLblClientsWaiting().setText(waiting);
	}

	/**
	 * przeslanie informacji na temat nastepnego, obecnego numeru do obslugi,
	 * czasu obslugi i oczekujacych klientow
	 */
	public static void sendToInformationApp() {
		server.service.sendNumberCurr(dataHandler.currNum);
		server.service.sendNumberNext(dataHandler.nextNum);
		server.service.sendClientTime(dataHandler.time);
		server.service.sendClientsWaiting(dataHandler.waiting);
	}

	public static CaseModel getDataModel() {
		return dataModel;
	}

	public static Server getServer() {
		return server;
	}

	/**
	 * proces usuwania i odswiezania danych w app
	 * 
	 * @param idTicket
	 *            numer usuwanej sprawy
	 */
	public static void deleteElement(long idTicket) {
		dataModel.removeElement(idTicket);
		dataHandler.update();
		sendToInformationApp();
	}

}
