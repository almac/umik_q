package frame;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

import network.Client;

public class InformationApp extends JFrame {

	/** panel do dodawania elementow graficznych */
	private JPanel contentPane;
	/** etykieta oczekujacych klientow */
	private static JLabel lblClientsWaiting;
	/** etykieta pokazujaca obecny czas */
	private JLabel lblCurrTime;
	/** etykieta pokazujaca obecnie oblugiwany numerek klienta */
	private static JLabel lblCurrNumber;
	/** etykieta pokazujaca nastepny oblugiwany numerek klienta */
	private static JLabel lblNextNumber;
	/** tykieta pokazujaca czas obslugi klienta */
	private static JLabel lblTimeInfo;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InformationApp frame = new InformationApp();
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Tworzenie aplikacji InformationApp
	 */
	public InformationApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 445, 253);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[] { 0, 0, 0 };
		gbl_contentPane.rowHeights = new int[] { 0, 65, 59, 0, 26, 0 };
		gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		contentPane.setLayout(gbl_contentPane);

		JLabel lblCurrNumberTitle = new JLabel("Obecny numer");
		GridBagConstraints gbc_lblCurrNumberTitle = new GridBagConstraints();
		gbc_lblCurrNumberTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblCurrNumberTitle.gridx = 0;
		gbc_lblCurrNumberTitle.gridy = 0;
		contentPane.add(lblCurrNumberTitle, gbc_lblCurrNumberTitle);

		JLabel lblNextNumberTitle = new JLabel("Nastepny numer");
		GridBagConstraints gbc_lblNextNumberTitle = new GridBagConstraints();
		gbc_lblNextNumberTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblNextNumberTitle.gridx = 1;
		gbc_lblNextNumberTitle.gridy = 0;
		contentPane.add(lblNextNumberTitle, gbc_lblNextNumberTitle);

		lblCurrNumber = new JLabel("1");
		lblCurrNumber.setFont(new Font("Georgia", Font.PLAIN, 50));
		GridBagConstraints gbc_lblCurrNumber = new GridBagConstraints();
		gbc_lblCurrNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblCurrNumber.gridx = 0;
		gbc_lblCurrNumber.gridy = 1;
		contentPane.add(lblCurrNumber, gbc_lblCurrNumber);

		lblNextNumber = new JLabel("1");
		lblNextNumber.setFont(new Font("Georgia", Font.PLAIN, 50));
		GridBagConstraints gbc_lblNextNumber = new GridBagConstraints();
		gbc_lblNextNumber.insets = new Insets(0, 0, 5, 0);
		gbc_lblNextNumber.gridx = 1;
		gbc_lblNextNumber.gridy = 1;
		contentPane.add(lblNextNumber, gbc_lblNextNumber);

		final SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");

		lblCurrTime = new JLabel(dt.format(new Date()).toString());
		lblCurrTime.setFont(new Font("Georgia", Font.PLAIN, 30));
		GridBagConstraints gbc_lblCurrTime = new GridBagConstraints();
		gbc_lblCurrTime.gridwidth = 2;
		gbc_lblCurrTime.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrTime.gridx = 0;
		gbc_lblCurrTime.gridy = 2;
		contentPane.add(lblCurrTime, gbc_lblCurrTime);

		ActionListener updateTimerAction = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblCurrTime.setText(dt.format(new Date()).toString());
			}
		};

		Timer timer = new Timer(1000, updateTimerAction);
		timer.start();

		JLabel lblClientsWaitingTitle = new JLabel("Liczba oczekujacych");
		GridBagConstraints gbc_lblClientsWaitingTitle = new GridBagConstraints();
		gbc_lblClientsWaitingTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblClientsWaitingTitle.gridx = 0;
		gbc_lblClientsWaitingTitle.gridy = 3;
		contentPane.add(lblClientsWaitingTitle, gbc_lblClientsWaitingTitle);

		lblClientsWaiting = new JLabel("100");
		GridBagConstraints gbc_lblClientsWaiting = new GridBagConstraints();
		gbc_lblClientsWaiting.insets = new Insets(0, 0, 5, 0);
		gbc_lblClientsWaiting.gridx = 1;
		gbc_lblClientsWaiting.gridy = 3;
		contentPane.add(lblClientsWaiting, gbc_lblClientsWaiting);

		JLabel lblTimeInfoTitle = new JLabel("Dlugosc obslugi 1 klienta");
		GridBagConstraints gbc_lblTimeInfoTitle = new GridBagConstraints();
		gbc_lblTimeInfoTitle.insets = new Insets(0, 0, 0, 5);
		gbc_lblTimeInfoTitle.gridx = 0;
		gbc_lblTimeInfoTitle.gridy = 4;
		contentPane.add(lblTimeInfoTitle, gbc_lblTimeInfoTitle);

		lblTimeInfo = new JLabel("05:33");
		GridBagConstraints gbc_lblTimeInfo = new GridBagConstraints();
		gbc_lblTimeInfo.gridx = 1;
		gbc_lblTimeInfo.gridy = 4;
		contentPane.add(lblTimeInfo, gbc_lblTimeInfo);

		try {
			Client client = new Client();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public static void updateCurrentNumber(String val) {
		lblCurrNumber.setText(val);
	}

	public static void updateNextNumber(String val) {
		lblNextNumber.setText(val);
	}

	public static void updateTimeInfo(String val) {
		lblTimeInfo.setText(val);
	}

	public static void updateClientsWaiting(String val) {
		lblClientsWaiting.setText(val);
	}

}
