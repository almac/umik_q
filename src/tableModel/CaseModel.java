package tableModel;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import tableData.CaseData;

public class CaseModel extends AbstractTableModel {

	/**
	 * tablica z nazwami kolumn w tabeli
	 */
	private final Object[] columnNames = { "Numer", "Typ sprawy", "Imie", "Nazwisko", "Inne" };
	/**
	 * wektor do przechowywania danych w tabeli
	 */
	private Vector<CaseData> caseVec;

	/**
	 * konstruktor klasy obrazujacej model danych spraw w tabeli
	 */
	public CaseModel() {
		caseVec = new Vector<>();
		setData();
	}

	private void setData() {
//		caseVec.add(new CaseData(1234124, 1, "paczka", "Adam", "Adamski", "ARW555", "xxx", "xxx"));
//		caseVec.add(new CaseData(1341341, 2, "list", "Marcin", "Marcinkowski", "AK345", "xxx", "xxx"));

	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return caseVec.size();
	}

	public boolean isCellEditable(int nRow, int nCol) {
		return false;
	}

	public String getColumnName(int column) {
		String str = (String) columnNames[column];
		return str;
	}

	@Override
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return "";
		CaseData row = (CaseData) caseVec.elementAt(nRow);
		switch (nCol) {
		case 0:
			return row.getNumber();
		case 1:
			return row.getCaseTitle();
		case 2:
			return row.getName();
		case 3:
			return row.getSurname();
		case 4:
			return row.getData1();

		}
		return "";
	}

	public Object getRow(int nRow) {
		if (nRow < 0 || nRow >= getRowCount())
			return "";
		CaseData row = (CaseData) caseVec.elementAt(nRow);
		return row;
	}

	public Vector<CaseData> getCaseVec() {
		return caseVec;
	}

	/**
	 * usuwa z modelu danych odpowiednia sprawe
	 * 
	 * @param idTicket
	 *            numer sprawy przeznaczony do usuniecia
	 */
	public void removeElement(long idTicket) {
		for (CaseData Case : caseVec) {
			if (Case.getIdTicket() == idTicket) {
				caseVec.remove(Case);
				fireTableDataChanged();
				break;
			}
		}
	}

}
