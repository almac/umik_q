package dialogs;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import frame.OfficeApp;

public class NextClientDialog extends JDialog {

	/** panel do dodawania elementow graficznych */
	private final JPanel contentPanel = new JPanel();
	/** checkbox sprawdzajacy stan obslugi klienta */
	private JCheckBox chckbxClientVisit;
	/** obiekt wywolujacy Jdialog */
	private OfficeApp frame;

	/**
	 * Konstruktor Jdialogu odpowiadajacego za sprawdzenie czy klient zostal
	 * obsluzony
	 * 
	 * @param owner
	 *            obiekt wywolujacy Jdialog
	 */
	public NextClientDialog(JFrame owner) {
		super(owner, "Nastepny klient", true);
		setBounds(100, 100, 307, 214);
		frame = (OfficeApp) owner;
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] { 0, 0 };
		gbl_contentPanel.rowHeights = new int[] { 88, 0, 0 };
		gbl_contentPanel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_contentPanel.rowWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblTitle = new JLabel("Potwierdzenie obslugi nastepnego klienta");
			GridBagConstraints gbc_lblTitle = new GridBagConstraints();
			gbc_lblTitle.insets = new Insets(0, 0, 5, 0);
			gbc_lblTitle.gridx = 0;
			gbc_lblTitle.gridy = 0;
			contentPanel.add(lblTitle, gbc_lblTitle);
		}
		{
			chckbxClientVisit = new JCheckBox("Klient nie pojawil sie");
			chckbxClientVisit.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (chckbxClientVisit.isSelected()==true){
						OfficeApp.dataHandler.setClientStatusServed(true);
						System.out.println("status: "+OfficeApp.dataHandler.clientServedStatus);
					}
						
					else
						OfficeApp.dataHandler.setClientStatusServed(false);
				}
			});
			chckbxClientVisit.setSelected(false);
			GridBagConstraints gbc_chckbxClientVisit = new GridBagConstraints();
			gbc_chckbxClientVisit.gridx = 0;
			gbc_chckbxClientVisit.gridy = 1;
			contentPanel.add(chckbxClientVisit, gbc_chckbxClientVisit);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Ok");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						frame.nextClicked();
						NextClientDialog.this.dispose();
					}
				});
				okButton.setActionCommand("Ok");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Anuluj");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						NextClientDialog.this.dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

}
