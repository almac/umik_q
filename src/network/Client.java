package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	/**
	 * Okre�la buforowany czytnik linii u�ywany do przetwarzania danych
	 * przychodz�cych z serwera.
	 */
	private BufferedReader reader;
	/**
	 * Okre�la obiekt klasy PrintWriter odpowiedzialny za wypisywanie tekstu na
	 * wyj�cie.
	 */
	private PrintWriter writer;
	/**
	 * Okre�la obiekt klasy InputStreamReader odpowiedzialny za odbieranie
	 * tekstu.
	 */
	private InputStreamReader stream;
	/** Okre�la gniazdko klienta. */
	private Socket client;

	private ClientService service;

	/**
	 * Klasa reprezentujaca usluge sieciowa uzywana przy komunikacji OfficeApp i
	 * InformationApp. Obsluguje wiadomosci wyslane przez OfficeApp, przetwarza
	 * je i wyswietla.
	 * 
	 * @throws Exception
	 */
	public Client() throws Exception {
		try {
			client = new Socket("127.0.0.1", 61113);
		} catch (UnknownHostException e) {
			throw new Exception("Unknown host.");
		} catch (IOException e) {
			throw new Exception("IO exception while connecting to the server.");
		} catch (NumberFormatException e) {
			throw new Exception("Port value must be a number.");
		}
		try {
			stream = new InputStreamReader(client.getInputStream());
			reader = new BufferedReader(stream);
			writer = new PrintWriter(client.getOutputStream(), true);
		} catch (IOException ex) {
			throw new Exception("Can not get input/output connection stream.");
		}
		send(Protocol.CONNECTION_REQUEST);
		service = new ClientService(reader);
		Thread receiver = new Thread(service);
		receiver.start();
	}

	/**
	 * wysyla wiadomosc na wyjscie
	 * 
	 * @param message
	 *            wysylana wiadomosc
	 */
	public void send(String message) {
		try {
			writer.println(message);
			writer.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
