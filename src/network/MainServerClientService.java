package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import tableData.CaseData;
import frame.OfficeApp;

public class MainServerClientService implements Runnable {

	private PrintWriter writer;
	private BufferedReader reader;

	/**tworzy wlasciwa usluge klienta polaczonego do glownego servera
	 * @param wr
	 *            PrintWirter
	 * @param re
	 *            BufferedReader
	 */
	public MainServerClientService(PrintWriter wr, BufferedReader re) {
		writer = wr;
		reader = re;
	}

	@Override
	public void run() {
		String command;
		try {
			
			while ((command = reader.readLine()) != "connection_closed") {
				System.out.println(command);
				switch (command) {
				case Protocol.SERVER_BEGIN_ELEMENT:
					collectData();
					break;
				
				case Protocol.SERVER_DELETE_BEGIN:
					deleteElement();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	

	/**
	 * informuje OfficeApp aby usunal sprawe o danym idTicket
	 */
	private void deleteElement() {
		String line;
		long idTicket;
		try {
			while (!Protocol.SERVER_DELETE_END.equals(line = reader.readLine())) {
				idTicket= Long.parseLong(line);
				OfficeApp.deleteElement(idTicket);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * pobiera informacje z glownego servera systemu
	 */
	private void collectData() {
		String line;
		try {
			while (!Protocol.SERVER_END_ELEMENT.equals(line = reader.readLine())) {
				String[] elements = line.split(";");
				// id, numer,imie, nazwisko, data 1, data 2, data 3 ,sprawa
				OfficeApp
						.getDataModel()
						.getCaseVec()
						.add(new CaseData(Long.parseLong(elements[0]), Integer.parseInt(elements[1]), elements[7], elements[2], elements[3], elements[4],
								elements[5], elements[6]));
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		OfficeApp.getDataModel().fireTableDataChanged();
		if (OfficeApp.dataHandler.isClientConnected()) {
			OfficeApp.dataHandler.update();
			OfficeApp.sendToInformationApp();
		}
	}

	/**przesylanie wiadomosci
	 * @param message wiadomosc ktora ma zostac przeslana
	 * 
	 */
	public void send(String message) {
		writer.println(message);
		writer.flush();
	}

	/**
	 * przeslanie informacji o obsluzonym numerze ticketa, statusie, czasu obslugi na MainServer
	 */
	public void sendNextNumber() {
		send("");
		if(OfficeApp.getDataModel().getCaseVec().size()!=0)
			send(Protocol.SERVER_SEND_NEXT_NUMBER+";"+OfficeApp.getDataModel().getCaseVec().get(0).getIdTicket() + ";" + OfficeApp.dataHandler.clientServedStatus + ";" + OfficeApp.dataHandler.time);
			///
//			if(OfficeApp.dataHandler.clientServedStatus)
//				OfficeApp.dataHandler.clientServedStatus=false;
//			else
//				OfficeApp.dataHandler.clientServedStatus=true;
		
		//else
//			send("-999" + ";" + OfficeApp.dataHandler.clientServedStatus + ";" + OfficeApp.dataHandler.time);
	}

}
