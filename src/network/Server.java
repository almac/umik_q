package network;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {

	public ServerService service;
	private ServerSocket serverSocket;

	/**
	 * Klasa reprezentujaca sieciowa obsluge czesci systemu kolejkowania w
	 * biurze. komunikuje OfficeApp i InformationApp
	 */
	public Server() {

		try {
			serverSocket = new ServerSocket(61113);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {

		while (true) {

			try {

				Socket clientSocket = serverSocket.accept();
				PrintWriter writer = new PrintWriter(clientSocket.getOutputStream());
				service = new ServerService(clientSocket, writer);
				Thread t = new Thread(service);
				t.start();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
