package network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import frame.OfficeApp;

public class ServerService implements Runnable {

	private PrintWriter writer;
	/** Okre�la bufor do wczytywania wiadomo�ci. */
	BufferedReader reader;
	/** Okre�la u�ywane gniazdko. */
	Socket socket;

	/**
	 * Tworzony jest obiekt reprezentujacy usluge serwera aplikacji OfficeApp
	 * 
	 * @param clientSocket
	 *            gniazdko klienta
	 * @param inpt
	 *            obiekt do pisania w strumieniu
	 */
	public ServerService(Socket clientSocket, PrintWriter inpt) {
		try {
			writer = inpt;
			socket = clientSocket;
			InputStreamReader stream = new InputStreamReader(socket.getInputStream());
			reader = new BufferedReader(stream);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run() {
		String command;
		try {
			while ((command = reader.readLine()) != null) {
				String[] entry = command.split(";");
				String header = entry[0];
				System.out.println(command);
				switch (header) {
				case Protocol.CONNECTION_REQUEST:
					OfficeApp.sendToInformationApp();
					OfficeApp.dataHandler.setClientConnected(true);
					break;
				case Protocol.CONNECTION_DISCONNECTED:
					OfficeApp.dataHandler.setClientConnected(false);
					break;

				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @param message
	 *            wiadomosc ktora podlega wyslaniu
	 */
	public void send(String message) {
		try {
			writer.println(message);
			writer.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * @param number
	 *            numer obecnego klienta wysylany do InformationApp
	 */
	public void sendNumberCurr(String number) {
		send(Protocol.NUMBER_CURRENT_UPDATE);
		send(number);
		send(Protocol.OBTAINED_NUMBER_CURRENT_UPDATE);
	}

	/**
	 * @param number
	 *            numer kolejnego klienta wysylany do InformationApp
	 */
	public void sendNumberNext(String number) {
		send(Protocol.NUMBER_NEXT_UPDATE);
		send(number);
		send(Protocol.OBTAINED_NUMBER_NEXT_UPDATE);
	}

	/**
	 * @param number
	 *            liczba oczekujacych klientow wysylana do InformationApp
	 */
	public void sendClientsWaiting(String number) {
		send(Protocol.CLIENT_WAITING_UPDATE);
		send(number);
		send(Protocol.OBTAINED_CLIENT_WAITING_UPDATE);
	}

	/**
	 * @param number
	 *            sredni czas oczekiwania kolejnego klienta wysylany do
	 *            InformationApp
	 */
	public void sendClientTime(String number) {
		send(Protocol.CLIENT_TIME_UPDATE);
		send(number);
		send(Protocol.OBTAINED_CLIENT_TIME_UPDATE);
	}

}
