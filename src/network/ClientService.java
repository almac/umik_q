package network;

import java.io.BufferedReader;
import java.io.IOException;

import frame.InformationApp;

public class ClientService implements Runnable {

	private BufferedReader reader;

	/**
	 * Klasa reprezentujaca wlasciwa usluge sieciowa obslugiwana przez
	 * InformationApp
	 * 
	 * @param re
	 *            bufferreader
	 */
	public ClientService(BufferedReader re) {
		reader = re;
	}

	@Override
	public void run() {
		String command;
		try {

			while ((command = reader.readLine()) != "connection_closed") {
				System.out.println(command);
				switch (command) {
				case Protocol.NUMBER_CURRENT_UPDATE:
					updateCurrentNumber();
					break;

				case Protocol.NUMBER_NEXT_UPDATE:
					updateNextNumber();
					break;

				case Protocol.CLIENT_WAITING_UPDATE:
					updateClientWaiting();
					break;

				case Protocol.CLIENT_TIME_UPDATE:
					updateClientTime();
					break;

				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * polecenie odswiezenia informacji o czasu obslugi klienta
	 */
	private void updateClientTime() {
		String line;
		try {
			while (!Protocol.OBTAINED_CLIENT_TIME_UPDATE.equals(line = reader.readLine())) {
				InformationApp.updateTimeInfo(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * polecenie odswiezenia informacji o ilosci oczekujacych klientow
	 */
	private void updateClientWaiting() {
		String line;
		try {
			while (!Protocol.OBTAINED_CLIENT_WAITING_UPDATE.equals(line = reader.readLine())) {
				InformationApp.updateClientsWaiting(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * polecenie odswiezenia informacji o obsludze nastepnego numerka
	 */
	private void updateNextNumber() {
		String line;
		try {
			while (!Protocol.OBTAINED_NUMBER_NEXT_UPDATE.equals(line = reader.readLine())) {
				InformationApp.updateNextNumber(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * polecenie odswiezenia informacji o obsludze obecnego numerka
	 */
	private void updateCurrentNumber() {
		String line;
		try {
			while (!Protocol.OBTAINED_NUMBER_CURRENT_UPDATE.equals(line = reader.readLine())) {
				InformationApp.updateCurrentNumber(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
