package network;

public class Protocol {

	public final static String CLIENT_WAITING_UPDATE = "client_waiting_update";
	public final static String CLIENT_TIME_UPDATE = "client_time_update";
	public final static String NUMBER_CURRENT_UPDATE = "number_current_update";
	public final static String NUMBER_NEXT_UPDATE = "number_next_update";

	public final static String CONNECTION_REQUEST = "connection_request";
	public static final String CONNECTION_DISCONNECTED = "connection_disconnected";

	public final static String OBTAINED_CLIENT_WAITING_UPDATE = "obtained_client_waiting_update";
	public final static String OBTAINED_CLIENT_TIME_UPDATE = "obtained_client_time_update";
	public final static String OBTAINED_NUMBER_CURRENT_UPDATE = "obtained_number_current_update";
	public final static String OBTAINED_NUMBER_NEXT_UPDATE = "obtained_number_next_update";

	// komunikacja MainServer<->OfficeApp

	public final static String SERVER_SEND_NEXT_NUMBER = "next_number";
	public final static String SERVER_BEGIN_ELEMENT = "element_begin";
	public final static String SERVER_END_ELEMENT = "element_end";
	public final static String SERVER_DELETE_BEGIN = "delete_begin";
	public final static String SERVER_DELETE_END = "delete_end";

}
