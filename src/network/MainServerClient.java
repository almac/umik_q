package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class MainServerClient {
	/**
	 * Okre�la buforowany czytnik linii u�ywany do przetwarzania danych
	 * przychodz�cych z serwera.
	 */
	private BufferedReader reader;
	/**
	 * Okre�la obiekt klasy PrintWriter odpowiedzialny za wypisywanie tekstu na
	 * wyj�cie.
	 */
	private PrintWriter writer;
	/**
	 * Okre�la obiekt klasy InputStreamReader odpowiedzialny za odbieranie
	 * tekstu.
	 */
	private InputStreamReader stream;
	/** Okre�la gniazdko klienta. */
	private Socket client;

	public MainServerClientService service;

	/**
	 * tworzy kliencki socket i laczy sie do MainServera na porcie 61111
	 * 
	 * @throws Exception
	 *             wyjatek przy tworzeniu polaczenia
	 */
	public MainServerClient() throws Exception {
		try {
			client = new Socket("87.207.252.141", 61112);//"87.207.252.141", 61112);
		} catch (UnknownHostException e) {
			throw new Exception("Unknown host.");
		} catch (IOException e) {
			throw new Exception("IO exception while connecting to the server.");
		} catch (NumberFormatException e) {
			throw new Exception("Port value must be a number.");
		}
		try {
			stream = new InputStreamReader(client.getInputStream());
			reader = new BufferedReader(stream);
			writer = new PrintWriter(client.getOutputStream(), true);
		} catch (IOException ex) {
			throw new Exception("Can not get input/output connection stream.");
		}

		sendHello();
		service = new MainServerClientService(writer, reader);
		Thread receiver = new Thread(service);
		receiver.start();
	}

	/**
	 * @param message
	 *            wiadomosc jaka ma zostac wyslana
	 */
	public void send(String message) {
		try {
			writer.println(message);
			writer.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * wiadomosc powitalna przy polaczeniu do MainServera
	 */
	public void sendHello() {
		send("");
		send("Hello;11");
	}

}
