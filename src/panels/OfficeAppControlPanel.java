package panels;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

public class OfficeAppControlPanel extends JPanel {

	/**
	 * etykieta przedstawiajaca obecnie obslugiwany numerek
	 */
	private JLabel lblCurrNumber;
	/**
	 * etykieta przedstawiajaca liczbe obsluzonych klientow
	 */
	private JLabel lblClientsServed;
	/**
	 * etykieta przedstawiajaca liczbe nieobsluzonych klientow
	 */
	private JLabel lblClientsNonServed;
	/**
	 * etykieta przedstawiajaca liczbe oczekujacych klientow
	 */
	private JLabel lblClientsWaiting;

	public OfficeAppControlPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 131, 132, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 79, 0, 0, 0, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblCurrNumTitle = new JLabel("Numer:");
		GridBagConstraints gbc_lblCurrNumTitle = new GridBagConstraints();
		gbc_lblCurrNumTitle.gridwidth = 2;
		gbc_lblCurrNumTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrNumTitle.gridx = 0;
		gbc_lblCurrNumTitle.gridy = 0;
		add(lblCurrNumTitle, gbc_lblCurrNumTitle);

		lblCurrNumber = new JLabel("--");
		lblCurrNumber.setFont(new Font("Georgia", Font.PLAIN, 45));
		GridBagConstraints gbc_lblCurrNumber = new GridBagConstraints();
		gbc_lblCurrNumber.gridwidth = 2;
		gbc_lblCurrNumber.insets = new Insets(0, 0, 5, 0);
		gbc_lblCurrNumber.gridx = 0;
		gbc_lblCurrNumber.gridy = 1;
		add(lblCurrNumber, gbc_lblCurrNumber);

		JSeparator separator = new JSeparator();
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 2;
		gbc_separator.insets = new Insets(0, 0, 5, 0);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 2;
		add(separator, gbc_separator);

		JLabel lblClientsServedTitle = new JLabel("Liczba obsluzonych");
		GridBagConstraints gbc_lblClientsServedTitle = new GridBagConstraints();
		gbc_lblClientsServedTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblClientsServedTitle.gridx = 0;
		gbc_lblClientsServedTitle.gridy = 3;
		add(lblClientsServedTitle, gbc_lblClientsServedTitle);

		lblClientsServed = new JLabel("0");
		GridBagConstraints gbc_lblClientsServed = new GridBagConstraints();
		gbc_lblClientsServed.insets = new Insets(0, 0, 5, 0);
		gbc_lblClientsServed.gridx = 1;
		gbc_lblClientsServed.gridy = 3;
		add(lblClientsServed, gbc_lblClientsServed);

		JLabel lblClientsNonServedTitl = new JLabel("Liczba nieobsluzonych");
		GridBagConstraints gbc_lblClientsNonServedTitl = new GridBagConstraints();
		gbc_lblClientsNonServedTitl.insets = new Insets(0, 0, 5, 5);
		gbc_lblClientsNonServedTitl.gridx = 0;
		gbc_lblClientsNonServedTitl.gridy = 4;
		add(lblClientsNonServedTitl, gbc_lblClientsNonServedTitl);

		lblClientsNonServed = new JLabel("0");
		GridBagConstraints gbc_lblClientsNonServed = new GridBagConstraints();
		gbc_lblClientsNonServed.insets = new Insets(0, 0, 5, 0);
		gbc_lblClientsNonServed.gridx = 1;
		gbc_lblClientsNonServed.gridy = 4;
		add(lblClientsNonServed, gbc_lblClientsNonServed);

		JLabel lblClientsWaitingTitle = new JLabel("Liczba oczekujacych");
		GridBagConstraints gbc_lblClientsWaitingTitle = new GridBagConstraints();
		gbc_lblClientsWaitingTitle.insets = new Insets(0, 0, 5, 5);
		gbc_lblClientsWaitingTitle.gridx = 0;
		gbc_lblClientsWaitingTitle.gridy = 5;
		add(lblClientsWaitingTitle, gbc_lblClientsWaitingTitle);

		lblClientsWaiting = new JLabel("--");
		GridBagConstraints gbc_lblClientsWaiting = new GridBagConstraints();
		gbc_lblClientsWaiting.insets = new Insets(0, 0, 5, 0);
		gbc_lblClientsWaiting.gridx = 1;
		gbc_lblClientsWaiting.gridy = 5;
		add(lblClientsWaiting, gbc_lblClientsWaiting);

	}

	public JLabel getLblCurrNumber() {
		return lblCurrNumber;
	}

	public void setLblCurrNumber(JLabel lblCurrNumber) {
		this.lblCurrNumber = lblCurrNumber;
	}

	public JLabel getLblClientsServed() {
		return lblClientsServed;
	}

	public void setLblClientsServed(JLabel lblClientsServed) {
		this.lblClientsServed = lblClientsServed;
	}

	public JLabel getLblClientsNonServed() {
		return lblClientsNonServed;
	}

	public void setLblClientsNonServed(JLabel lblClientsNonServed) {
		this.lblClientsNonServed = lblClientsNonServed;
	}

	public JLabel getLblClientsWaiting() {
		return lblClientsWaiting;
	}

	public void setLblClientsWaiting(JLabel lblClientsWaiting) {
		this.lblClientsWaiting = lblClientsWaiting;
	}
}
