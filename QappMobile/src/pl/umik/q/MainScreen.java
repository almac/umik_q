package pl.umik.q;

import pl.umik.q.app.QappMobile;
import pl.umik.q.database.DatabaseHandler;
import pl.umik.q.gps.GpsEvent;
import pl.umik.q.network.NetworkService;
import pl.umik.q.util.QFragmentPagerAdapter;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class MainScreen extends SherlockFragmentActivity {// implements
															// LocationListener
															// {

	private ActionBar mActionBar;
	private ViewPager mPager;
	private DatabaseHandler db;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);

		mActionBar = getSupportActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		mPager = (ViewPager) findViewById(R.id.pager);
		FragmentManager fm = getSupportFragmentManager();
		ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
			public void onPageSelected(int position) {
				super.onPageSelected(position);
				mActionBar.setSelectedNavigationItem(position);
			}
		};

		mPager.setOnPageChangeListener(pageChangeListener);
		QFragmentPagerAdapter fragmentPagerAdapter = new QFragmentPagerAdapter(fm);
		mPager.setAdapter(fragmentPagerAdapter);
		mActionBar.setDisplayShowTitleEnabled(true);

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			}

			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				mPager.setCurrentItem(tab.getPosition());
			}

			public void onTabReselected(Tab tab, FragmentTransaction ft) {
			}
		};

		Tab tab = mActionBar.newTab().setText("Lista spraw").setTabListener(tabListener);
		mActionBar.addTab(tab);
		tab = mActionBar.newTab().setText("Aktywne bilety").setTabListener(tabListener);
		mActionBar.addTab(tab);

		// ///////////////

		db = QappMobile.db;
		QappMobile.main = this;
		QappMobile.gpsEvent = new GpsEvent(MainScreen.this);
		QappMobile.networkService = new NetworkService();
		QappMobile.networkService.execute();
	
		// Log.i("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",
		// "X: "+gpsEvent.getLatitude()+" "+"y: "+gpsEvent.getLongitude());
		// ///////////////////////////

		// QappMobile.networkService = new NetworkService();
		// QappMobile.networkService.execute();
		// Log.d("Reading: ", "Reading all contacts..");
		//
		// List<Place> places = db.getAllPlaces();
		// Profile profil = db.getProfile();
		//
		// String log = "Id: " + profil.getId() + " ,Name: " + profil.getName()
		// + " ,Sur: " + profil.getSurname();
		// Log.d("Profil: ", log);
		//
		// for (Place cn : places) {
		// String log2 = "Id: " + cn.getID() + " ,Name: " + cn.getName() +
		// " ,Address: " + cn.getAddress() + ",Geox: " + cn.getGeox() +
		// ",Geoy: "
		// + cn.getGeoy() + ",Distance: " + cn.getDistance();
		// // Writing Contacts to log
		// Log.d(cn.getType()+": ", log2);
		// }

	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// your stuff or nothing
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		// your stuff or nothing
	}

	// public void onLocationChanged(Location location) {
	// if (location.getLatitude() != 0)
	// latitude = location.getLatitude();
	// if (location.getLongitude() != 0)
	// longitude = location.getLongitude();
	// Toast.makeText(QappMobile.main, "Your Location is - \nLat: " + latitude +
	// "\nLong: " + longitude, Toast.LENGTH_LONG).show();
	// Log.i("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", "X: " + latitude + " " +
	// "y: " + longitude);
	// // TODO check if get to localisation
	//
	// }
	//
	// @Override
	// public void onProviderDisabled(String provider) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void onProviderEnabled(String provider) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// @Override
	// public void onStatusChanged(String provider, int status, Bundle extras) {
	// // TODO Auto-generated method stub
	//
	// }
	//
	// public double getLatitude() {
	// return latitude;
	// }
	//
	// public void setLatitude(double latitude) {
	// this.latitude = latitude;
	// }
	//
	// public double getLongitude() {
	// return longitude;
	// }
	//
	// public void setLongitude(double longitude) {
	// this.longitude = longitude;
	// }

}
