package pl.umik.q;

import pl.umik.q.app.QappMobile;
import pl.umik.q.database.DatabaseHandler;
import pl.umik.q.network.NetworkService;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class SplashScreen extends Activity {

	private static final int TIME = 2000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splashscreen);

		// Uruchom w�tek otwieraj�cy g��wn� aktywno��
		ActivityStarter starter = new ActivityStarter();
		starter.start();
	}

	private class ActivityStarter extends Thread {

		@Override
		public void run() {
			try {
				// tutaj wrzucamy wszystkie akcje potrzebne podczas �adowania
				// aplikacji
				//Intent intent = new Intent(SplashScreen.this, AndroidSQLiteTutorialActivity.class);
//				
				QappMobile.db = new DatabaseHandler(getApplicationContext());
				Thread.sleep(TIME);
			} catch (Exception e) {
				Log.e("SplashScreen", e.getMessage());
			}

			// W��cz g��wn� aktywno��
			Intent intent = new Intent(SplashScreen.this, MainScreen.class);
			SplashScreen.this.startActivity(intent);
			SplashScreen.this.finish();
		}
	}

}
