package pl.umik.q.gps;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.umik.q.app.QappMobile;
import pl.umik.q.data.Case;
import pl.umik.q.database.Place;
import pl.umik.q.util.TicketTask;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

public class GpsEvent implements LocationListener {
	private LocationManager locationManager;
	private double latitude;
	private double longitude;
	private Activity activity;

	/** czas co jaki aktualizujemy pomiar gpsem */
	private static final long TIME = 1;

	public GpsEvent(Activity ac) {

		activity = ac;

		locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

		// if (!isGPSEnabled && !isNetworkEnabled) {
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME, 0, this);
		// }
		// else

	}

	@Override
	public void onLocationChanged(Location location) {
		if (location.getLatitude() != 0)
			latitude = location.getLatitude();
		if (location.getLongitude() != 0)
			longitude = location.getLongitude();
		Toast.makeText(QappMobile.main, "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();

		if (!QappMobile.caseList.isEmpty()) {
			Set<String> set = new HashSet<String>();

			for (Case c : QappMobile.caseList) {
				set.add(c.getImage());
			}
			String columns = TextUtils.join("' , '", set);

			List<Place> places = QappMobile.db.getSpecificPlaces(columns);
			ArrayList<Place> potentialQueues = new ArrayList<Place>();
			for (Place p : places) {
				Location placeLoc = new Location(" ");
				placeLoc.setLatitude(p.getGeox());
				placeLoc.setLongitude(p.getGeoy());
				Log.i("GEOOOOOO", location.getLatitude() + " " + location.getLongitude() + " !!! " + placeLoc.distanceTo(location));

				if (placeLoc.distanceTo(location) < p.getDistance()) {
					potentialQueues.add(p);
				}
			}
			int position = -1;
			List<Case> toRemove= new ArrayList<Case>(); //TODO -1
			for (int i = 0; i < QappMobile.caseList.size(); i++) {
				for (Place p : potentialQueues) {
					if (QappMobile.caseList.get(i).getImage().equals(p.getType())) {
						QappMobile.networkService.sendAdd(p.getID(), QappMobile.db.getClientId(), "WIFI", QappMobile.caseList.get(i).getCaseType());
						toRemove.add(QappMobile.caseList.get(i));
					}
					
				}
			}
			for(Case c: toRemove){
				QappMobile.caseList.remove(c);
				QappMobile.caseAdapter.notifyDataSetChanged();
				//QappMobile.ticketAdapter.notifyDataSetChanged();
//				new TicketTask(QappMobile.listFragment.getActivity()).execute();
			}
		}

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
