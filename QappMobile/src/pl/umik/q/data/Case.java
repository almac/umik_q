package pl.umik.q.data;

public class Case {

	private String placeType;
	private String caseType;
	private String image;

	public Case(String placeType, String caseType, String image) {
		super();
		this.placeType = placeType;
		this.caseType = caseType;
		this.image = image;
	}

	public String getPlaceType() {
		return placeType;
	}

	public void setPlaceType(String placeType) {
		this.placeType = placeType;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
