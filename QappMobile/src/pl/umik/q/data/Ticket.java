package pl.umik.q.data;

public class Ticket {
	
	private long idQueue;
	private long idTicket;
	private String address;
	private String waitingTime;
	private String image;
	private int clientNumber;
	private int expectedNumber;
	
	
	public Ticket(long idQueue, long idTicket, String address, String waitingTime, String image, int clientNumber, int expectedNumber) {
		super();
		this.idQueue = idQueue;
		this.idTicket = idTicket;
		this.address = address;
		this.waitingTime = waitingTime;
		this.image = image;
		this.clientNumber = clientNumber;
		this.expectedNumber = expectedNumber;
	}

	public Ticket(String address, String waitingTime, String image, int clientNumber, int expectedNumber) {
		super();
		this.address = address;
		this.waitingTime = waitingTime;
		this.image = image;
		this.clientNumber = clientNumber;
		this.expectedNumber = expectedNumber;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(String waitingTime) {
		this.waitingTime = waitingTime;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(int clientNumber) {
		this.clientNumber = clientNumber;
	}
	public int getExpectedNumber() {
		return expectedNumber;
	}
	public void setExpectedNumber(int expectedNumber) {
		this.expectedNumber = expectedNumber;
	}

	public long getIdQueue() {
		return idQueue;
	}

	public void setIdQueue(long idQueue) {
		this.idQueue = idQueue;
	}

	public long getIdTicket() {
		return idTicket;
	}

	public void setIdTicket(long idTicket) {
		this.idTicket = idTicket;
	}

}
