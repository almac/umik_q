package pl.umik.q.data;

public class GpsPlace {
	
	private int idPlace;
	double metric;
	
	public GpsPlace(int idPlace, double metric) {
		super();
		this.idPlace = idPlace;
		this.metric = metric;
	}

	public int getIdPlace() {
		return idPlace;
	}

	public void setIdPlace(int idPlace) {
		this.idPlace = idPlace;
	}

	public double getMetric() {
		return metric;
	}

	public void setMetric(double metric) {
		this.metric = metric;
	}
	
	

}
