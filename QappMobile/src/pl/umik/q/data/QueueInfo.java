package pl.umik.q.data;

public class QueueInfo {

	private String id;
	private String name;
	private String address;
	private String serviceTime;
	private int inQueue;
	private double distance;
	private double metric;

	public QueueInfo(String id, String name, String address, String serviceTime, int inQueue, double distance) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.serviceTime = serviceTime;
		this.inQueue = inQueue;
		this.distance = distance;
		this.metric=0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(String serviceTime) {
		this.serviceTime = serviceTime;
	}

	public int getInQueue() {
		return inQueue;
	}

	public void setInQueue(int inQueue) {
		this.inQueue = inQueue;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getMetric() {
		return metric;
	}

	public void setMetric(double metric) {
		this.metric = metric;
	}

}
