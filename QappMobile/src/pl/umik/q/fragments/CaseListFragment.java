package pl.umik.q.fragments;

import java.util.ArrayList;
import java.util.List;

import pl.umik.q.ClosestPlace;
import pl.umik.q.R;
import pl.umik.q.UpdateProfileActivity;
import pl.umik.q.app.QappMobile;
import pl.umik.q.data.Case;
import pl.umik.q.data.QueueInfo;
import pl.umik.q.util.CaseListAdapter;
import pl.umik.q.util.QueueInfoListAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

/**
 * Klasa opisujaca jedna strone slajdera MainScreen, odpowiada za przechowywanie
 * listy oczekujacych spraw
 * 
 */
public class CaseListFragment extends SherlockListFragment {

	

	// ArrayAdapter<Case> ad = null;

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		QappMobile.caseList = new ArrayList<Case>();
		ArrayList<Case> caseList=QappMobile.caseList;
		caseList.add(new Case("Poczta", "paczka", "poczta"));
		caseList.add(new Case("Urzad", "dowod", "urzad"));
		caseList.add(new Case("Bank", "kredyt", "bank"));
//		caseList.add(new Case("Kino", "iron man 3", "kino"));
		caseList.add(new Case("Uczelnia", "kredyt", "uczelnia"));
//		caseList.add(new Case("Restauracja", "uczta", "restauracja"));
//		caseList.add(new Case("Praca", "rozmowa", "praca"));
		
		QappMobile.caseAdapter= new CaseListAdapter(getActivity(), R.layout.fragment_case_list_row, caseList);
		setListAdapter(QappMobile.caseAdapter);

		ListView lv = getListView();
		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> av, View v, final int pos, final long id) {

				Dialog dialog = createChoiceDialog(pos);
				dialog.show();
				return true;

			}
		});
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.fragment_case_list_menu, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.newCase:
			Dialog dialog = createNewCaseDialog();
			dialog.show();
			return true;
		case R.id.changeProfile:
			startActivity(new Intent(this.getActivity(), UpdateProfileActivity.class));
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * @param listPosition
	 * @return
	 */
	private Dialog createChoiceDialog(int listPosition) {
		final int position = listPosition;
		final ArrayAdapter<Case> ad = (ArrayAdapter<Case>) getListAdapter();
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_list);
		String[] listContent = { "Znajdz najblizsze obiekty", "Usun" };
		dialog.setTitle("Wybor");
		ListView choiceList = (ListView) dialog.findViewById(R.id.choice_list);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, listContent);
		choiceList.setAdapter(adapter);

		choiceList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
				if (pos == 0) {
					Intent intent = new Intent(getActivity(), ClosestPlace.class);
					intent.putExtra("place_type", QappMobile.caseList.get(position).getImage()); 
					startActivity(intent);
					dialog.dismiss();
				}

				else {
					Log.i("CHOICE DIALOG", "Clicked: " + pos);
					createDeleteDialog(position, ad);
					dialog.dismiss();
				}

			}

		});
		return dialog;
	}

	/** funkcja odpowiadajaca za utworzenie okienka dialogowego odpowiadajacego za usuniecie sprawy
	 * @param position miejsce na liscie spraw
	 * @param adapter adapter listy spraw
	 */
	protected void createDeleteDialog(final int position, final ArrayAdapter<Case> adapter) {

		final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());

		b.setMessage("Czy chcesz usunac te sprawe?");
		b.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Log.i("CaseListFragment", "Yes clicked, removed: " + position);
				QappMobile.caseList.remove(position);
				adapter.notifyDataSetChanged();
			}
		});
		b.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Log.i("CaseListFragment", "No clicked, on: " + position);
			}
		});

		b.show();

	}

	/** funkcja odpowiadajaca za utworzenie okienka dialogowego tworzacego nowa sprawe
	 * @return okienko dialogowe do utworzenia nowej sprawy
	 */
	private Dialog createNewCaseDialog() {
		final Dialog dialog = new Dialog(getActivity());
		dialog.setContentView(R.layout.dialog_create_case);
		dialog.setTitle("Nowa sprawa");

		final ArrayAdapter<Case> ad = (ArrayAdapter<Case>) getListAdapter();

		final Spinner placeSpinner = (Spinner) dialog.findViewById(R.id.spinner_choose_place);
		final Spinner caseSpinner = (Spinner) dialog.findViewById(R.id.spinner_choose_case);
		Button okButton = (Button) dialog.findViewById(R.id.button_dialog_case_ok);
		Button cancelButton = (Button) dialog.findViewById(R.id.button_dialog_case_cancel);

		ArrayAdapter<CharSequence> placeSpinnerAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.places_array,
				android.R.layout.simple_spinner_item);
		placeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ArrayAdapter<CharSequence> caseSpinnerAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.start_array,
				android.R.layout.simple_spinner_item);
		caseSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		final ArrayAdapter<CharSequence> postOfficeAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.case_postoffice_array,
				android.R.layout.simple_spinner_item);
		caseSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		final ArrayAdapter<CharSequence> cityHallAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.case_cityhall_array,
				android.R.layout.simple_spinner_item);
		caseSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		final ArrayAdapter<CharSequence> bankAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.case_bank_array,
				android.R.layout.simple_spinner_item);
		caseSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		final ArrayAdapter<CharSequence> collegeAdapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.case_college_array,
				android.R.layout.simple_spinner_item);
		caseSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		OnItemSelectedListener placeSpinnerListener = new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				switch (pos) {
				case 0:
					caseSpinner.setAdapter(postOfficeAdapter);
					break;
				case 1:
					caseSpinner.setAdapter(cityHallAdapter);
					break;
				case 2:
					caseSpinner.setAdapter(bankAdapter);
					break;
				case 3:
					caseSpinner.setAdapter(collegeAdapter);
					break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		};

		placeSpinner.setAdapter(placeSpinnerAdapter);
		placeSpinner.setOnItemSelectedListener(placeSpinnerListener);
		caseSpinner.setAdapter(caseSpinnerAdapter);

		okButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String place = placeSpinner.getSelectedItem().toString();
				String case_ = caseSpinner.getSelectedItem().toString();
				Log.i("DIALOG", "PLACE: " + place + " CASE: " + case_);
				QappMobile.caseList.add(new Case(place, case_, place.toLowerCase()));
				ad.notifyDataSetChanged();
				dialog.cancel();
			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		return dialog;
	}

}
