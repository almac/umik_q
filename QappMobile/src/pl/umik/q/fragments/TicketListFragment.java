package pl.umik.q.fragments;

import java.util.ArrayList;

import pl.umik.q.R;
import pl.umik.q.app.QappMobile;
import pl.umik.q.data.Ticket;
import pl.umik.q.util.TicketListAdapter;
import pl.umik.q.util.TicketTask;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

/**
 * Klasa opisujaca jedna karte slidera MainScreen, przechowuje aktywne bilety
 * 
 */
public class TicketListFragment extends SherlockListFragment {

	// ListView lv=null;
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setHasOptionsMenu(true);
		// zmiana list -> QappMobile.list
		// final List<Ticket> ticketList = new ArrayList<Ticket>();
		QappMobile.ticketList = new ArrayList<Ticket>();
//		QappMobile.ticketList.add(new Ticket(1342134, 1324, "pocztowa 37", "4:45", "poczta", 12, 23));
//		QappMobile.ticketList.add(new Ticket(12455, 1234, "urzedowa 12", "12;13", "urzad", 12, 12));
//		QappMobile.ticketList.add(new Ticket(1245, 5455, "bankowa 34", "42:12", "bank", 33, 13));
//		QappMobile.ticketList.add(new Ticket(123, 444, "kinowa 134", "3:50", "kino", 34, 3));
//		QappMobile.ticketList.add(new Ticket(4342, 12, "uczelniana 13", "123:00", "uczelnia", 12, 3));
//		QappMobile.ticketList.add(new Ticket(1234, 134, "restauracyjna 51", "12:33", "restauracja", 11, 2));
//		QappMobile.ticketList.add(new Ticket(5351, 41, "pracownicza ", "11:12", "praca", 12, 3));
		setListAdapter(new TicketListAdapter(getActivity(), R.layout.fragment_ticket_list_row, QappMobile.ticketList));

		QappMobile.listFragment=this;
		ListView lv = getListView();
		 QappMobile.ticketAdapter = (ArrayAdapter<Ticket>) getListAdapter();
		
		///
		//QappMobile.ticketAdapter =

		lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> av, View v, final int pos, final long id) {

				final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());

				b.setMessage("Czy chcesz usunac ten bilet?");
				b.setPositiveButton("Tak", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Log.i("TicketListFragment", "Yes clicked, removed: " + pos);
						QappMobile.networkService.sendDelete(QappMobile.ticketList.get(pos).getIdTicket());
						QappMobile.ticketList.remove(pos);
						
						QappMobile.ticketAdapter.notifyDataSetChanged();
					}
				});
				b.setNegativeButton("Nie", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Log.i("TicketListFragment", "No clicked, on: " + pos);
					}
				});

				b.show();

				return true;

			}
		});

	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.fragment_ticket_list_menu, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.refreshTicket:
			new TicketTask(this.getActivity()).execute();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

//	public class TicketTask extends AsyncTask<String, Void, Boolean> {
//		private Context context;
//		private ProgressDialog dialog;
//		private Activity activity;
//		//QueueInfoListAdapter adapter;
//
//		public TicketTask(Activity activity) {
//			this.activity = activity;
//			context = activity;
//			dialog = new ProgressDialog(context);
//		}
//
//		protected void onPreExecute() {
//			// ListView lv= TicketListFragment.getListView();
//			// adapter = new QueueInfoListAdapter(activity,
//			// R.layout.dialog_closest_place_row, QappMobile.queueInfoList);
//			// choiceList.setAdapter(adapter);
//			dialog = ProgressDialog.show(activity, "�adowanie..", "Odswiezanie biletow", true);
//		}
//
//		protected void onPostExecute(final Boolean success) {
//			
//			//adapter
//			QappMobile.ticketAdapter.notifyDataSetChanged();
//
//			if (dialog.isShowing()) {
//				dialog.dismiss();
//			}
//		}
//
//		protected Boolean doInBackground(final String... args) {
//			
//			int size = QappMobile.ticketList.size();
//			
//				Log.i("SIZ: ",Integer.toString(size));
//				for (Ticket t : QappMobile.ticketList){
//					QappMobile.networkService.sendCheck(t.getIdTicket());
//					
//				}
//				QappMobile.ticketList.clear();
//				
//				
//				while (QappMobile.ticketList.size() < size) {
//
//				}
//				
//			
//			return false;
//		}
//	}

}
