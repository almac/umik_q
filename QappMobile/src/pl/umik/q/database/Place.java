package pl.umik.q.database;

public class Place {

	// private variables
	private int _id;
	private String _type;
	private String _name;
	private String _address;
	private double _geox;
	private double _geoy;
	private double _distance;
	private String _wifi;

	// constructor
	public Place(){
		
	}
	
	public Place(String type, String name, String address, double geox, double geoy, double dist, String wifi) {
		this._type=type;
		this._name = name;
		this._address = address;
		this._geox = geox;
		this._geoy = geoy;
		this._distance = dist;
		this._wifi=wifi;
	}
	
	public Place(int id, String type, String name, String address, double geox, double geoy, double dist, String wifi) {
		this._id = id;
		this._type=type;
		this._name = name;
		this._address = address;
		this._geox = geox;
		this._geoy = geoy;
		this._distance = dist;
		this._wifi=wifi;
	}

	// getting ID
	public int getID() {
		return this._id;
	}

	// setting id
	public void setID(int id) {
		this._id = id;
	}

	// getting name
	public String getName() {
		return this._name;
	}

	// setting name
	public void setName(String name) {
		this._name = name;
	}

	// getting phone number
	public String getAddress() {
		return this._address;
	}

	// setting phone number
	public void setAddress(String address) {
		this._address = address;
	}

	// getting phone number
	public double getGeox() {
		return this._geox;
	}

	// setting phone number
	public void setGeox(double geox) {
		this._geox = geox;
	}

	// getting phone number
	public double getGeoy() {
		return this._geoy;
	}

	// setting phone number
	public void setGeoy(double geoy) {
		this._geoy = geoy;
	}

	// getting phone number
	public double getDistance() {
		return this._distance;
	}

	// setting phone number
	public void setDistance(double dist) {
		this._distance = dist;
	}

	public String getType() {
		return _type;
	}

	public void setType(String _type) {
		this._type = _type;
	}

	public String getWifi() {
		return _wifi;
	}

	public void setWifi(String _wifi) {
		this._wifi = _wifi;
	}
}