package pl.umik.q.database;

public class Profile {
	
	private long id;
	private String name;
	private String surname;
	private String data1;
	private String data2;
	private String data3;
	
	/**Konstruktor profilu uzytkownika
	 * @param id identyfikator usera
	 * @param name imie
	 * @param surname nazwisko
	 * @param data1 dane 1
	 * @param data2 dane 2
	 * @param data3 dane 3
	 */
	public Profile(long id, String name, String surname, String data1, String data2, String data3) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.data1 = data1;
		this.data2 = data2;
		this.data3 = data3;
	}

	public Profile() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public String getData3() {
		return data3;
	}

	public void setData3(String data3) {
		this.data3 = data3;
	}
	
	public String toString(){
		return id+". "+name+" "+surname+" "+data1+" "+data2+" "+data3;
		
	}
	

}
