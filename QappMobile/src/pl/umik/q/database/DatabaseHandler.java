package pl.umik.q.database;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Klasa obslugujaca baze i operacje na bazie danych
 */
public class DatabaseHandler extends SQLiteOpenHelper {

	/** wersja bazy danych */
	private static final int DATABASE_VERSION = 1;
	/** nazwa bazy danych */
	private static final String DATABASE_NAME = "placesDB";
	/** nazwa tabeli profilu */
	private static final String TABLE_PROFIL = "profil";
	/** nazwa tablicy miejsc */
	private static final String TABLE_PLACES = "places";
	/** kolumna id */
	private static final String KEY_ID = "id";
	/** kolumna typu miejsca */
	private static final String KEY_TYPE = "type";
	/** kolumna nazwy miejsca lub imienia uzytkownika */
	private static final String KEY_NAME = "name";
	/** kolumna adnresu miejsca */
	private static final String KEY_ADDRESS = "address";
	/** kolumna wsp szer geog miejsca */
	private static final String KEY_GEOX = "geox";
	/** kolumna szerokosci geog miejsca */
	private static final String KEY_GEOY = "geoy";
	/** kolumna prominia strefy wokol budynku */
	private static final String KEY_DISTANCE = "distance";
	/** kolumna nazwiiska uzytkownika */
	private static final String KEY_SURNAME = "surname";
	/** kolumna pola danych 1 */
	private static final String KEY_DATA1 = "data1";
	/** kolumna pola danych 2 */
	private static final String KEY_DATA2 = "data2";
	/** kolumna pola danych 3 */
	private static final String KEY_DATA3 = "data3";
	/** kolumna nazwy wifi w budynku */
	private static final String KEY_WIFI = "wifi";

	/**
	 * Konstruktor klasy
	 * 
	 * @param context
	 *            kontekst aplikacji
	 */
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

//		db.execSQL("DROP TABLE " + TABLE_PLACES);
//		db.execSQL("DROP TABLE " + TABLE_PROFIL);
		String CREATE_PLACES_TABLE = "CREATE TABLE " + TABLE_PLACES + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TYPE + " TEXT," + KEY_NAME + " TEXT,"
				+ KEY_ADDRESS + " TEXT," + KEY_GEOX + " VARCHAR," + KEY_GEOY + " VARCHAR," + KEY_DISTANCE + " VARCHAR," + KEY_WIFI + " TEXT" + ")";
		db.execSQL(CREATE_PLACES_TABLE);

		String CREATE_PROFIL_TABLE = "CREATE TABLE " + TABLE_PROFIL + "(" + KEY_ID + " INTEGER," + KEY_NAME + " TEXT," + KEY_SURNAME + " TEXT," + KEY_DATA1
				+ " TEXT," + KEY_DATA2 + " TEXT," + KEY_DATA3 + " TEXT" + ")";
		db.execSQL(CREATE_PROFIL_TABLE);
		
//		String DEL = "DELETE FROM "+TABLE_PLACES;
//		db.execSQL(DEL);

		populateProfile(db, new Profile(0, "name", "surname", "data1", "data2", "data3"));

		populatePlace(db, new Place("urzad", "warszawa �r�dmie�cie", "Nowogrodzka 43", 52.228248, 21.010544, 80, "brak"));
		populatePlace(db, new Place("urzad", "warszawa ochota", "Gr�jecka 17a", 52.222080, 20.987168, 20, "brak"));
		populatePlace(db, new Place("urzad", "warszawa mokot�w", "Aleja Niepodleglosci 151", 52.205312, 21.009056, 80, "brak"));
		populatePlace(db, new Place("urzad", "warszawa wilan�w", "Potockeigo 11", 52.235072, 21.034880, 80, "brak"));
		populatePlace(db, new Place("urzad", "urzad miasta stolecznego warszawy", "Plac Bankowy 3", 52.242624, 21.001952, 80, "brak"));

		populatePlace(db, new Place("bank", "Deutche Bank PBC", "Aleja Armii Ludowej 26", 52.217504, 21.008186, 80, "brak"));
		populatePlace(db, new Place("bank", "Citibank Handlowy", "Polna 11", 52.218392, 21.014066, 80, "brak"));
		populatePlace(db, new Place("bank", "ING Bank �l�ski", "Nowowiejska 5", 52.219804, 21.015116, 80, "brak"));
		populatePlace(db, new Place("bank", "PKO oddzia� 92", "Wary�skiego 28", 52.220976, 21.015836, 80, "brak"));
		populatePlace(db, new Place("bank", "Polbank EFG", "Mokotowska 19", 52.220320, 21.018044, 80, "brak"));

		populatePlace(db, new Place("uczelnia", "PW - EITI", "Nowowiejska 15", 52.219824, 21.011444, 80, "brak"));
		populatePlace(db, new Place("uczelnia", "PW - gmach g��wny", "Plac Politechniki 1", 52.220328, 21.010742, 80, "brak"));
		populatePlace(db, new Place("uczelnia", "PW - In�ynieria �rodowiska", "Nowowiejska 20", 52.220228, 21.008636, 80, "brak"));
		populatePlace(db, new Place("uczelnia", "UW - Fizyka", "Ho�a 69", 52.224112, 21.004856, 80, "brak"));
		populatePlace(db, new Place("uczelnia", "PW - Architektura", "Koszykowa 55", 52.222608, 21.014000, 80, "brak"));

		populatePlace(db, new Place("poczta", "FUP Warszawa 10", "Aleja Armii Ludowej 26", 52.217426, 21.008152, 80, "brak"));
		populatePlace(db, new Place("poczta", "UP Warszawa 10", "Plac Konstytucji 3", 52.221824, 21.015692, 80, "brak"));
		populatePlace(db, new Place("poczta", "FUP Warszawa 81", "Tytusa Cha�ubi�skiego 4", 52.223824, 21.004592, 80, "brak"));
		populatePlace(db, new Place("poczta", "UP Warszawa 1", "�wi�tokrzyska 31/33", 52.235360, 21.010598, 80, "brak"));
		populatePlace(db, new Place("poczta", "FUP Warszawa 133", "Wsp�lna 30", 52.227520, 21.015128, 80, "brak"));
		
		populatePlace(db, new Place("poczta", "test", "testowa 666", 52.2160762, 21.0160465, 100, "brak")); 

//	db.close();

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTOFFICE);
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_BANK);
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_COLLEGE);
		// db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFICE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLACES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFIL);
		// Create tables again
		onCreate(db);
	}

	/**
	 * Metoda do zapelniania bazy danych profilu przy pierwszym uruchomieniu
	 * 
	 * @param db
	 *            baza danych
	 * @param profile
	 *            dane profilu
	 */
	private void populateProfile(SQLiteDatabase db, Profile profile) {
		ContentValues values = new ContentValues();

		values.put(KEY_ID, profile.getId());
		values.put(KEY_NAME, profile.getName());
		values.put(KEY_SURNAME, profile.getSurname());
		values.put(KEY_DATA1, profile.getData1());
		values.put(KEY_DATA2, profile.getData2());
		values.put(KEY_DATA3, profile.getData3());

		// Inserting Row
		db.insert(TABLE_PROFIL, null, values);
	}

	/**
	 * Metoda do zapelniania bazy danych miejsc przy pierwszym uruchomieniu
	 * 
	 * @param db
	 *            baza danych
	 * @param place
	 *            dane miejsca
	 */
	public void populatePlace(SQLiteDatabase db, Place place) {
		ContentValues values = new ContentValues();
		values.put(KEY_TYPE, place.getType());
		values.put(KEY_NAME, place.getName()); // Contact Name
		values.put(KEY_ADDRESS, place.getAddress()); // Contact Phone
		values.put(KEY_GEOX, place.getGeox()); // Contact Phone
		values.put(KEY_GEOY, place.getGeoy()); // Contact Phone
		values.put(KEY_DISTANCE, place.getDistance()); // Contact Phone
		values.put(KEY_WIFI, place.getWifi());
		// values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone

		// Inserting Row
		db.insert(TABLE_PLACES, null, values);
	}

	/**
	 * Metoda do dodania nowego miejsca
	 * 
	 * @param place
	 *            dane miejsca
	 */
	public void addPlace(Place place) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TYPE, place.getType());
		values.put(KEY_NAME, place.getName()); // Contact Name
		values.put(KEY_ADDRESS, place.getAddress()); // Contact Phone
		values.put(KEY_GEOX, place.getGeox()); // Contact Phone
		values.put(KEY_GEOY, place.getGeoy()); // Contact Phone
		values.put(KEY_DISTANCE, place.getDistance()); // Contact Phone
		values.put(KEY_WIFI, place.getWifi());
		// values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone

		// Inserting Row
		db.insert(TABLE_PLACES, null, values);
		db.close(); // Closing database connection
	}

	/**
	 * Metoda wyszukania miejsca o danym id
	 * 
	 * @param id
	 *            identyfikator miejsca
	 * @return dane miejsca
	 */
	Place getPlace(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_PLACES, new String[] { KEY_ID, KEY_TYPE, KEY_NAME, KEY_ADDRESS, KEY_GEOX, KEY_GEOY, KEY_DISTANCE, KEY_WIFI }, KEY_ID
				+ "=?", new String[] { String.valueOf(id) }, null, null, null, null);
		if (cursor != null)
			cursor.moveToFirst();

		Place place = new Place(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), Double.parseDouble(cursor
				.getString(4)), Double.parseDouble(cursor.getString(5)), Double.parseDouble(cursor.getString(6)), cursor.getString(7));
		// return contact
		cursor.close();
		db.close();
		return place;
	}

	/**
	 * Metoda do wybrania danych profilu
	 * 
	 * @return zwraca profil
	 */
	public Profile getProfile() {
		String selectQuery = "SELECT  * FROM " + TABLE_PROFIL;

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(selectQuery, null);
		Profile profile = new Profile();
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				profile.setId(Long.parseLong(cursor.getString(0)));
				profile.setName(cursor.getString(1));
				profile.setSurname(cursor.getString(2));
				profile.setData1(cursor.getString(3));
				profile.setData2(cursor.getString(4));
				profile.setData3(cursor.getString(5));
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		db.close();
		return profile;
	}
	
	public long getClientId() {
		String selectQuery = "SELECT id FROM " + TABLE_PROFIL;

		SQLiteDatabase db = this.getWritableDatabase();

		Cursor cursor = db.rawQuery(selectQuery, null);
		long id=0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				id=Long.parseLong(cursor.getString(0));
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		db.close();
		return id;
	}

	/**
	 * Metoda do zwrocenia wszystkich miejsc
	 * 
	 * @return zwraca wszystkie miejsca
	 */
	public List<Place> getAllPlaces() {
		List<Place> contactList = new ArrayList<Place>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_PLACES;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Place place = new Place();
				place.setID(Integer.parseInt(cursor.getString(0)));
				place.setType(cursor.getString(1));
				place.setName(cursor.getString(2));
				place.setAddress(cursor.getString(3));
				place.setGeox(Double.parseDouble(cursor.getString(4)));
				place.setGeoy(Double.parseDouble(cursor.getString(5)));
				place.setDistance(Double.parseDouble(cursor.getString(6)));
				place.setWifi(cursor.getString(7));
				// Adding contact to list
				contactList.add(place);
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		db.close();
		return contactList;
	}
	
	public List<Place> getSpecificPlaces( String string) {
		List<Place> contactList = new ArrayList<Place>();
		// Select All Query
		String selectQuery = "SELECT  * FROM "+ TABLE_PLACES+ " WHERE "+ KEY_TYPE+" IN ( '" + string +"')";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Place place = new Place();
				place.setID(Integer.parseInt(cursor.getString(0)));
				place.setType(cursor.getString(1));
				place.setName(cursor.getString(2));
				place.setAddress(cursor.getString(3));
				place.setGeox(Double.parseDouble(cursor.getString(4)));
				place.setGeoy(Double.parseDouble(cursor.getString(5)));
				place.setDistance(Double.parseDouble(cursor.getString(6)));
				place.setWifi(cursor.getString(7));
				// Adding contact to list
				contactList.add(place);
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		db.close();
		return contactList;
	}
	
//	public List<Place> getSomePlaces(Set s){
//		
//		
//		return list;
//	}
	
	public List<Place> getAllSpecialPlaces(String type) {
		List<Place> contactList = new ArrayList<Place>();
		// Select All Query
		
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(TABLE_PLACES, new String[] { KEY_ID, KEY_TYPE, KEY_NAME, KEY_ADDRESS, KEY_GEOX, KEY_GEOY, KEY_DISTANCE, KEY_WIFI }, KEY_TYPE
				+ "=?", new String[] { type }, null, null, null, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Place place = new Place();
				place.setID(Integer.parseInt(cursor.getString(0)));
				place.setType(cursor.getString(1));
				place.setName(cursor.getString(2));
				place.setAddress(cursor.getString(3));
				place.setGeox(Double.parseDouble(cursor.getString(4)));
				place.setGeoy(Double.parseDouble(cursor.getString(5)));
				place.setDistance(Double.parseDouble(cursor.getString(6)));
				place.setWifi(cursor.getString(7));
				// Adding contact to list
				contactList.add(place);
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		db.close();
		return contactList;
	}

	/**
	 * Metoda do aktualizacji miejsca
	 * 
	 * @param place
	 *            dane miejsca
	 * @return zwraca flage po dodaniu
	 */
	public void updatePlace(Place place) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TYPE, place.getType());
		values.put(KEY_NAME, place.getName());
		values.put(KEY_ADDRESS, place.getAddress());
		values.put(KEY_GEOX, place.getGeox());
		values.put(KEY_GEOY, place.getGeox());
		values.put(KEY_DISTANCE, place.getDistance());
		values.put(KEY_WIFI, place.getWifi());

		// updating row
		db.update(TABLE_PLACES, values, KEY_ID + " = ?", new String[] { String.valueOf(place.getID()) });
		db.close();
	}

	/**
	 * metoda do aktualizacji profilu
	 * 
	 * @param name
	 *            imie
	 * @param surname
	 *            nazwisko
	 * @param data1
	 *            dane 1
	 * @param data2
	 *            dane 2
	 * @param data3
	 *            dane 3
	 * @return zwraca flage dodania
	 */
	public void updateProfile(String name, String surname, String data1, String data2, String data3) {
		String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_PROFIL;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		long key = 0;
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				key = Long.parseLong(cursor.getString(0));
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name);
		values.put(KEY_SURNAME, surname);
		values.put(KEY_DATA1, data1);
		values.put(KEY_DATA2, data2);
		values.put(KEY_DATA3, data3);

		// updating row
		db.update(TABLE_PROFIL, values, KEY_ID + " = ?", new String[] { Long.toString(key) });
		db.close();

	}

	/**
	 * Metoda do zmiany id klienta
	 * 
	 * @param newKey
	 *            nowe id klienta
	 * @return
	 * @return flaga dodania
	 */
	public void updateProfileKey(long newKey) {
		SQLiteDatabase db = this.getWritableDatabase();

		String selectQuery = "SELECT  * FROM " + TABLE_PROFIL;
		Cursor cursor = db.rawQuery(selectQuery, null);
		Profile profile = new Profile();
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				profile.setId(Long.parseLong(cursor.getString(0)));
				profile.setName(cursor.getString(1));
				profile.setSurname(cursor.getString(2));
				profile.setData1(cursor.getString(3));
				profile.setData2(cursor.getString(4));
				profile.setData3(cursor.getString(5));
			} while (cursor.moveToNext());
		}

		// return contact list
		cursor.close();
		ContentValues values = new ContentValues();
		values.put(KEY_ID, newKey);
		values.put(KEY_NAME, profile.getName());
		values.put(KEY_SURNAME, profile.getSurname());
		values.put(KEY_DATA1, profile.getData1());
		values.put(KEY_DATA2, profile.getData2());
		values.put(KEY_DATA3, profile.getData3());

		// updating row
		db.update(TABLE_PROFIL, values, KEY_ID + " = ?", new String[] { String.valueOf(profile.getId()) });
		db.close();
	}

	/**
	 * Metoda do usuniecia miejsca o danym id
	 * 
	 * @param place
	 *            id miejsca
	 */
	public void deletePlace(Place place) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_PLACES, KEY_ID + " = ?", new String[] { String.valueOf(place.getID()) });
		db.close();
	}

}