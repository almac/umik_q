package pl.umik.q;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.umik.q.app.QappMobile;
import pl.umik.q.data.GpsPlace;
import pl.umik.q.data.QueueInfo;
import pl.umik.q.database.Place;
import pl.umik.q.util.GpsPlaceComparator;
import pl.umik.q.util.QueueInfoComparator;
import pl.umik.q.util.QueueInfoListAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class ClosestPlace extends Activity {

	QueueInfoListAdapter adapter;
	boolean test = true;
	String place;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_list);
		QappMobile.queueInfoList = new ArrayList<QueueInfo>();
		place = getIntent().getStringExtra("place_type");
		new ProgressTask(ClosestPlace.this).execute();

	}

	private class ProgressTask extends AsyncTask<String, Void, Boolean> {
		private Context context;
		private ProgressDialog dialog;
		private Activity activity;
		QueueInfoListAdapter adapter;

		public ProgressTask(Activity activity) {
			this.activity = activity;
			context = activity;
			dialog = new ProgressDialog(context);
		}

		protected void onPreExecute() {
			ListView choiceList = (ListView) findViewById(R.id.choice_list);
			adapter = new QueueInfoListAdapter(activity, R.layout.dialog_closest_place_row, QappMobile.queueInfoList);
			choiceList.setAdapter(adapter);
			dialog = ProgressDialog.show(ClosestPlace.this, "�adowanie..", "Pobieranie najblizszych miejsc", true);
		}

		protected void onPostExecute(final Boolean success) {
			adapter.notifyDataSetChanged();

			if (dialog.isShowing()) {
				dialog.dismiss();
			}
		}

		protected Boolean doInBackground(final String... args) {
			List<Place> places = QappMobile.db.getAllSpecialPlaces(place);
			// dane z gps.
			// najblizsze, oblicz odl.
			// double gpsX = 52.21793;
			// double gpsY = 21.01579;
			
//			Location placeLoc = new Location(" ");
//			placeLoc.setLatitude(p.getGeox());
//			placeLoc.setLongitude(p.getGeoy());
//			Log.i("GEOOOOOO", location.getLatitude() + " " + location.getLongitude() + " !!! " + placeLoc.distanceTo(location));

//			if (placeLoc.distanceTo(location)

			ArrayList<GpsPlace> arrayList = new ArrayList<GpsPlace>();
			for (Place p : places) {
				Location placeLoc = new Location(" ");
				placeLoc.setLatitude(p.getGeox());
				placeLoc.setLongitude(p.getGeoy());
				
				Location placeLoc2 = new Location(" ");
				placeLoc2.setLatitude(QappMobile.gpsEvent.getLatitude());
				placeLoc2.setLongitude(QappMobile.gpsEvent.getLongitude());
				
				arrayList.add(new GpsPlace(p.getID(), placeLoc.distanceTo(placeLoc2)) );
			}

			// Log.i("CLOSE", "X: " + QappMobile.gpsEvent.getLatitude() + " " +
			// "y: " + QappMobile.gpsEvent.getLongitude());
			// for (GpsPlace gp : arrayList)
			// Log.i("GPS", Double.toString(gp.getMetric()));

			Collections.sort(arrayList, new GpsPlaceComparator());
			// Log.i("GPS", "LOLOLOLOLOLOL");
			// for (GpsPlace gp : arrayList)
			// Log.i("GPS", "Id: " + gp.getIdPlace() + "Metryka: " +
			// Double.toString(gp.getMetric()));

			if (QappMobile.queueInfoList.size() > 0) {
				for (QueueInfo q : QappMobile.queueInfoList)
					QappMobile.queueInfoList.remove(q);
			}

			QappMobile.networkService.sendAsk(arrayList.get(0).getIdPlace());
			QappMobile.networkService.sendAsk(arrayList.get(1).getIdPlace());
			QappMobile.networkService.sendAsk(arrayList.get(2).getIdPlace());
			while (QappMobile.queueInfoList.size() < 3) {

			}
			int t = 0;
			for (QueueInfo qi : QappMobile.queueInfoList) {
				qi.setMetric(calculateMetric(qi));
				qi.setDistance((double) arrayList.get(t).getMetric());
				t++;
				// Log.i("Comparator: ",qi.getName()+" "+
				// Double.toString(qi.getMetric()));
			}

			Collections.sort(QappMobile.queueInfoList, new QueueInfoComparator());
			return false;
		}

		private double calculateMetric(QueueInfo q) {
			String[] elems = q.getServiceTime().split(":");
			double secs = Integer.parseInt(elems[0]) * 60 + Integer.parseInt(elems[1]);
			return (q.getDistance() + 0.3 * q.getInQueue() + 0.6 * secs);
		}

		private double checkDistance(double X, double Y, double x, double y) {
			double dist = Math.sqrt(Math.pow(X - x, 2) + Math.pow(Y - y, 2));
			return dist;
		}
	}
}
