package pl.umik.q.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import pl.umik.q.app.QappMobile;
import pl.umik.q.data.QueueInfo;
import pl.umik.q.data.Ticket;
import pl.umik.q.database.Place;
import pl.umik.q.database.Profile;
import android.os.AsyncTask;
import android.util.Log;

public class NetworkService extends AsyncTask {
	/**
	 * Okre�la buforowany czytnik linii u�ywany do przetwarzania danych
	 * przychodz�cych z serwera.
	 */
	private BufferedReader reader;
	/**
	 * Okre�la obiekt klasy PrintWriter odpowiedzialny za wypisywanie tekstu na
	 * wyj�cie.
	 */
	private PrintWriter writer;
	/**
	 * Okre�la obiekt klasy InputStreamReader odpowiedzialny za odbieranie
	 * tekstu.
	 */
	private InputStreamReader stream;
	/** Okre�la gniazdko klienta. */
	private Socket client;

	@Override
	protected Object doInBackground(Object... params) {
		// try {
		Log.i("AsyncTask", "doInBackground: Creating socket");
		try {
			client = new Socket();
			SocketAddress sa = new InetSocketAddress(QappMobile.ip, QappMobile.port);// "10.0.2.2"
			client.connect(sa, 5000);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			stream = new InputStreamReader(client.getInputStream());
			reader = new BufferedReader(stream);
			writer = new PrintWriter(client.getOutputStream(), true);

		} catch (IOException e) {
			e.printStackTrace();
		}
		Log.i("AsyncTask", "doInBackground: Socket created, streams assigned");

		sendHello();
		String command;
		try {

			while ((command = reader.readLine()) != "connection_closed") {
				String[] elements = command.split(";");
//				Log.i("CHUCHUHCUJ: ",Integer.toString(elements.length));
//				Log.i("! ! ! SERVER:", command);
				if (elements[0].equals(Protocol.GET_HELLO)) {
					getHello(elements);

				}
				if (elements[0].equals(Protocol.GET_ASK)) {
					getAsk(elements);
				}
				if (elements[0].equals(Protocol.GET_ADD)) {
					getAdd(elements);
				}
				if (elements[0].equals(Protocol.GET_QUEUE)) {
					getQueue(elements);
				}
				if (elements[0].equals(Protocol.GET_CHECK)) {
					getCheck(elements);
				}

			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Log.i("AsyncTask", "doInBackground: ENDS");
		return null;
	}

	/**
	 * funkcja aktualizuje numer id klienta po otrzymaniu go z servera
	 * 
	 * @param idClient
	 *            nowy numer identyfikacyjny klienta
	 */
	public void getHello(String[] elements) {
		Log.i("INTERNAT: ", elements[0] + " " + elements[1]);
		QappMobile.db.updateProfileKey(Integer.parseInt(elements[1]));
	}

	/**
	 * funkcja dodajaca nowe elementy listy kolejek, do kt�rych mo�e zapisa� si�
	 * telefon
	 * 
	 * @param elements
	 *            parametry miejsca: (id kolejki) kategoria; nazwa; adres;
	 *            latitude; longtitude; promien; wifi
	 */
	public void getQueue(String[] elements) {
		QappMobile.db.addPlace(new Place(elements[2], elements[3], elements[4], Double.parseDouble(elements[5]), Double.parseDouble(elements[6]), Double
				.parseDouble(elements[7]), elements[8]));
	}

	/**
	 * Funkcja zwracajaca informacje o stanie kolejki po uprzednim zapytaniu do
	 * najblizszych kolejek
	 * 
	 * @param elements
	 *            tablica danych do obrobienia
	 * @return zwraca informacje o stanie kolejki id_kolejka;numer;liczba;czas
	 */
	public void getAsk(String[] elements) {
		Log.i("GET ASK SIZE: ",Integer.toString(elements.length));
		int i=0;
		for(String s : elements){
			
			Log.i("ELEM", i+s);
			i++;
		}
		if(QappMobile.queueInfoList ==null)
			QappMobile.queueInfoList = new ArrayList<QueueInfo>();
			//Log.i("euyheiwweifhewfiu", "sdsd");
		
		//Log.i("! ! ! SERVER:", elements.toString());
		QappMobile.queueInfoList.add(new QueueInfo(elements[1], elements[2], elements[3], elements[6], Integer.parseInt(elements[5]), Double
				.parseDouble(elements[4])));
			
	}

	/**
	 * Server przesyla nowy bilet
	 * 
	 * @param elements
	 *            parametry nowego aktywnego biletu
	 */
	//long idQueue, long idTicket, String address, String waitingTime, String image, int clientNumber, int expectedNumber
	//\r\nask;id_kolejka;nazwa;adres;numer;liczba;czas\r\n
	public void getAdd(String[] elements) {
		QappMobile.ticketList.add(new Ticket(Long.parseLong(elements[1]), Long.parseLong(elements[2]), elements[3], elements[4], elements[5], Integer
				.parseInt(elements[6]), Integer.parseInt(elements[6])));
		//QappMobile.ticketAdapter.notifyDataSetChanged();
	}

	/**
	 * @param elements
	 *            informacje na temat statusu biletu
	 */
	public void getCheck(String[] elements) {
		QappMobile.ticketList.add(new Ticket(Long.parseLong(elements[1]), Long.parseLong(elements[2]), elements[3], elements[4], elements[5], Integer
				.parseInt(elements[6]), Integer.parseInt(elements[7])));
	}

	/**
	 * funkcja przesyla informacje na temat uzytkownika do servera
	 */
	private void sendHello() {
		Profile profile = QappMobile.db.getProfile();
		send("hello;" + profile.getName() + ";" + profile.getSurname() + ";" + profile.getData1() + ";" + profile.getData2() + ";"
				+ profile.getData3());
	}

	/**
	 * funkcja pytajaca o stan konkretnej kolejki
	 * 
	 * @param id
	 *            kolejki
	 */
	public void sendAsk(long id) {
		send("ask;" + id);
	}

	/**
	 * funkcja zgloszenia checi pobrania biletu
	 * 
	 * @param idQueue
	 *            id kolejki
	 * @param idClient
	 *            id klienta
	 * @param cause
	 *            wyzwalacz GPS/WIFI
	 * @param Case
	 *            sprawa jaka chce zalatwic klient
	 */
	public void sendAdd(long idQueue, long idClient, String cause, String Case) {
		send("add;" + idQueue + ";" + idClient + ";" + cause + ";" + Case);
	}

	/**
	 * funkcja w ktorej uzytkownik informuje o usunieciu biletu
	 * 
	 * @param idTicket
	 *            id usuwanego ticketa
	 */
	public void sendDelete(long idTicket) {
		send("delete;" + idTicket);
	}

	public void sendCheck(long idTicket) {
		send("check;" + idTicket);
	}

	public void send(String message) {
		try {
			writer.println("");
			writer.println(message);
			writer.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
