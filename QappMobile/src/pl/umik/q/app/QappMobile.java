package pl.umik.q.app;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockListFragment;

import pl.umik.q.MainScreen;
import pl.umik.q.data.Case;
import pl.umik.q.data.QueueInfo;
import pl.umik.q.data.Ticket;
import pl.umik.q.database.DatabaseHandler;
import pl.umik.q.gps.GpsEvent;
import pl.umik.q.network.NetworkService;
import pl.umik.q.util.CaseListAdapter;
import pl.umik.q.util.QueueInfoListAdapter;
import android.app.Application;
import android.widget.ArrayAdapter;

public class QappMobile extends Application {

	public static DatabaseHandler db = null;
	public static String ip = "192.168.1.9";//"192.168.1.8";//87.207.252.141";
	public static int port = 61111;
	public static List<Ticket> ticketList = null;
	public static NetworkService networkService = null;
	public static ArrayList<QueueInfo> queueInfoList = null;
	public static ArrayList<Case> caseList;
	public static MainScreen main = null;
	public static GpsEvent gpsEvent = null;
	public static CaseListAdapter caseAdapter= null;
	public static ArrayAdapter<Ticket> ticketAdapter= null;
	public static SherlockListFragment listFragment= null;
}
