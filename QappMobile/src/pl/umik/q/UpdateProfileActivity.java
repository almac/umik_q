package pl.umik.q;

import pl.umik.q.app.QappMobile;
import pl.umik.q.database.Profile;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class UpdateProfileActivity extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_updateprofil);

		Profile profile = QappMobile.db.getProfile();

		final EditText profileName = (EditText) findViewById(R.id.lbl_profile_name);
		profileName.setHint(profile.getName());
		final EditText profileSurname = (EditText) findViewById(R.id.lbl_profile_surname);
		profileSurname.setHint(profile.getSurname());
		final EditText profileData1 = (EditText) findViewById(R.id.profile_data1);
		profileData1.setHint(profile.getData1());
		final EditText profileData2 = (EditText) findViewById(R.id.profile_data2);
		profileData2.setHint(profile.getData2());
		final EditText profileData3 = (EditText) findViewById(R.id.profile_data3);
		profileData3.setHint(profile.getData3());

		Button button = (Button) findViewById(R.id.profile_ok);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				QappMobile.db.updateProfile(profileName.getText().toString(), profileSurname.getText().toString(), profileData1.getText().toString(),
						profileData2.getText().toString(), profileData3.getText().toString());
				finish();
			}
		});
	}

}
