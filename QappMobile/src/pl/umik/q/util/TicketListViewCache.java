package pl.umik.q.util;

import pl.umik.q.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TicketListViewCache {

	private View baseView;
	private TextView textClientNumber;
	private TextView textExpectedNumberTime;
	private TextView textAddress;
	private ImageView placeImage;

	public TicketListViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}
	
	public TextView getTextClientNumber(int resource) {
		if (textClientNumber == null) {
			textClientNumber = (TextView) baseView.findViewById(R.id.ticketNumber);
		}
		return textClientNumber;
	}

	public TextView getTextExpectedNumberTime(int resource) {
		if (textExpectedNumberTime == null) {
			textExpectedNumberTime = (TextView) baseView.findViewById(R.id.otherInfo);
		}
		return textExpectedNumberTime;
	}

	public TextView getTextAddress(int resource) {
		if (textAddress == null) {
			textAddress = (TextView) baseView.findViewById(R.id.address);
		}
		return textAddress;
	}

	public ImageView getImageView(int resource) {
		if (placeImage == null) {
			placeImage = (ImageView) baseView.findViewById(R.id.ticketPlaceImage);
		}
		return placeImage;
	}
}
