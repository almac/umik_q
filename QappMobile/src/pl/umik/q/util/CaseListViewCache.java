package pl.umik.q.util;

import pl.umik.q.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CaseListViewCache {

	private View baseView;
	private TextView textPlaceType;
	private TextView textCaseType;
	private ImageView placeImage;

	public CaseListViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextPlaceType(int resource) {
		if (textPlaceType == null) {
			textPlaceType = (TextView) baseView.findViewById(R.id.placeType);
		}
		return textPlaceType;
	}

	public TextView getTextCaseType(int resource) {
		if (textCaseType == null) {
			textCaseType = (TextView) baseView.findViewById(R.id.caseType);
		}
		return textCaseType;
	}

	public ImageView getImageView(int resource) {
		if (placeImage == null) {
			placeImage = (ImageView) baseView.findViewById(R.id.casePlaceImage);
		}
		return placeImage;
	}
}
