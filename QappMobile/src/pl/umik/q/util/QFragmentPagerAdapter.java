package pl.umik.q.util;


import pl.umik.q.fragments.CaseListFragment;
import pl.umik.q.fragments.TicketListFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class QFragmentPagerAdapter extends FragmentStatePagerAdapter  {
final int PAGE_COUNT = 2;
	
	public QFragmentPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
	    Bundle data = new Bundle();
        switch(arg0){
               case 0:
                CaseListFragment casesList = new CaseListFragment();
                data.putInt("current_page", arg0+1);
                casesList.setArguments(data);
                return casesList;
 
            
            case 1:
                TicketListFragment ticketList = new TicketListFragment();
                data.putInt("current_page", arg0+1);
                ticketList.setArguments(data);
                return ticketList;
        }
        return null;
	}

	@Override
	public int getCount() {
		return PAGE_COUNT;
	}
}
