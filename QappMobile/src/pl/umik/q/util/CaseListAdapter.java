package pl.umik.q.util;

import java.util.List;

import pl.umik.q.data.Case;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CaseListAdapter extends ArrayAdapter<Case> {

	private int resource;
	private LayoutInflater inflater;
	private Context context;

	public CaseListAdapter(Context _context, int textViewResourceId, List<Case> objects) {
		super(_context, textViewResourceId, objects);
		resource = textViewResourceId;
		inflater = LayoutInflater.from(_context);
		context = _context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Case Case = getItem(position);
		CaseListViewCache viewCache;

		if (convertView == null) {
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			viewCache = new CaseListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (CaseListViewCache) convertView.getTag();
		}

		TextView textPlaceType = viewCache.getTextPlaceType(position);
		textPlaceType.setText(Case.getPlaceType());

		TextView textCaseType = viewCache.getTextCaseType(position);
		textCaseType.setText(Case.getCaseType());

		ImageView imageCity = viewCache.getImageView(resource);
		String uri = "drawable/" + Case.getImage();
		int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
		Drawable image = context.getResources().getDrawable(imageResource);
		imageCity.setImageDrawable(image);
		return convertView;
	}
}