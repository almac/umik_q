package pl.umik.q.util;

import java.util.List;

import pl.umik.q.data.QueueInfo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class QueueInfoListAdapter extends ArrayAdapter<QueueInfo> {

	private int resource;
	private LayoutInflater inflater;
	private Context context;

	public QueueInfoListAdapter(Context _context, int textViewResourceId, List<QueueInfo> objects) {
		super(_context, textViewResourceId, objects);
		resource = textViewResourceId;
		inflater = LayoutInflater.from(_context);
		context = _context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		QueueInfo queueItem = getItem(position);
		QueueInfoListViewCache viewCache;

		if (convertView == null) {
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			viewCache = new QueueInfoListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (QueueInfoListViewCache) convertView.getTag();
		}

		TextView textPlaceName = viewCache.getTextPlaceName(position);
		textPlaceName.setText(queueItem.getName());
		TextView textPlaceAddress = viewCache.getTextPlaceAddress(position);
		textPlaceAddress.setText(queueItem.getAddress());
		TextView textInQueue = viewCache.getTextInQueue(position);
		textInQueue.setText(Integer.toString(queueItem.getInQueue()));
		TextView textServiceTime = viewCache.getTextServiceTime(position);
		textServiceTime.setText(queueItem.getServiceTime());
		TextView textDistance = viewCache.getTextDistance(position);
		textDistance.setText(Double.toString(queueItem.getDistance()));

		return convertView;
	}
}