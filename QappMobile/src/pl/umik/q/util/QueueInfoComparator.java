package pl.umik.q.util;

import java.util.Comparator;

import pl.umik.q.data.QueueInfo;

public class QueueInfoComparator implements Comparator<QueueInfo> {
	/**
	 * Metoda s�u��ca do por�wnywania dw�ch metryk
	 * 
	 * @param arg0
	 *            Okre�la jedna lokalizacje, b�d�cy obiektem por�wnywania.
	 * @param arg1
	 *            Okre�la drugi lokalizacje, b�d�cy obiektem por�wnywania.
	 * @return R�nica mi�dzy uzyskanymi metrykami.
	 * 
	 */

	public int compare(QueueInfo c1, QueueInfo c2) {
		return new Double(c1.getMetric()).compareTo(new Double(c2.getMetric()));
	}
}
