package pl.umik.q.util;

import pl.umik.q.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class QueueInfoListViewCache {

	private View baseView;
	private TextView textPlaceName;
	private TextView textPlaceAddress;
	private TextView textInQueue;
	private TextView textServiceTime;
	private TextView textDistance;

	public QueueInfoListViewCache(View baseView) {
		this.baseView = baseView;
	}

	public View getViewBase() {
		return baseView;
	}

	public TextView getTextPlaceName(int resource) {
		if (textPlaceName == null) {
			textPlaceName = (TextView) baseView.findViewById(R.id.lbl_place_name);
		}
		return textPlaceName;
	}
	
	public TextView getTextPlaceAddress(int resource) {
		if (textPlaceAddress == null) {
			textPlaceAddress = (TextView) baseView.findViewById(R.id.lbl_place_address);
		}
		return textPlaceAddress;
	}
	
	public TextView getTextInQueue(int resource) {
		if (textInQueue == null) {
			textInQueue = (TextView) baseView.findViewById(R.id.lbl_in_queue);
		}
		return textInQueue;
	}
	
	public TextView getTextServiceTime(int resource) {
		if (textServiceTime == null) {
			textServiceTime = (TextView) baseView.findViewById(R.id.lbl_service_time);
		}
		return textServiceTime;
	}
	
	public TextView getTextDistance(int resource) {
		if (textDistance == null) {
			textDistance = (TextView) baseView.findViewById(R.id.lbl_distance);
		}
		return textDistance;
	}


}
