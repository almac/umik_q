package pl.umik.q.util;

import java.util.List;

import pl.umik.q.data.Case;
import pl.umik.q.data.Ticket;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TicketListAdapter extends ArrayAdapter<Ticket> {

	private int resource;
	private LayoutInflater inflater;
	private Context context;

	public TicketListAdapter(Context _context, int textViewResourceId, List<Ticket> objects) {
		super(_context, textViewResourceId, objects);
		resource = textViewResourceId;
		inflater = LayoutInflater.from(_context);
		context = _context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		Ticket ticket = getItem(position);
		TicketListViewCache viewCache;

		if (convertView == null) {
			convertView = (RelativeLayout) inflater.inflate(resource, null);
			viewCache = new TicketListViewCache(convertView);
			convertView.setTag(viewCache);
		} else {
			convertView = (RelativeLayout) convertView;
			viewCache = (TicketListViewCache) convertView.getTag();
		}

		TextView textClientNumber = viewCache.getTextClientNumber(position);
		textClientNumber.setText("Twoj numer " + ticket.getClientNumber());
		TextView textExpectedNumberTime = viewCache.getTextExpectedNumberTime(position);
		textExpectedNumberTime.setText("Pozostalo " + ticket.getExpectedNumber() + ", " + ticket.getWaitingTime());
		TextView textAddress = viewCache.getTextAddress(position);
		textAddress.setText(ticket.getAddress());
		ImageView placeImage = viewCache.getImageView(resource);
		String uri = "drawable/" + ticket.getImage();
		int imageResource = context.getResources().getIdentifier(uri, null, context.getPackageName());
		Drawable image = context.getResources().getDrawable(imageResource);
		placeImage.setImageDrawable(image);

		return convertView;
	}
}